const mongoose = require('mongoose');

const addressSchema = new mongoose.Schema({
  id: { type: Number, unique: true },
  line: { type: String },
  city: { type: String },
  state: { type: String },
  country: { type: String },
  postcode: { type: String }

}, { timestamps: true });


const Address = mongoose.model('Address', addressSchema);

module.exports = Address;
