const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  email: { type: String, unique: true },
  password: String,
  passwordResetToken: String,
  passwordResetExpires: Date,

  facebook: String,
  twitter: String,
  google: String,
  github: String,
  instagram: String,
  linkedin: String,
  steam: String,
  tokens: Array,

  profile: {
    name: String,
    gender: String,
    location: String,
    website: String,
    picture: String,
    occupation: String,
    company: String,
    phone: String
  },

  role: Object,

  address: {
    line: { type: String },
    city: { type: String },
    state: { type: String },
    country: { type: String },
    postcode: { type: String }
  },

  reviews: Array,
  publications: Array,

}, { timestamps: true });

/**
 * Password hash middleware.
 */
userSchema.pre('save', function save(next) {
  const user = this;
  if (!user.isModified('password')) { return next(); }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) { return next(err); }
    bcrypt.hash(user.password, salt, null, (err, hash) => {
      if (err) { return next(err); }
      user.password = hash;
      next();
    });
  });
});

/**
 * Helper method for validating user's password.
 */
userSchema.methods.comparePassword = function comparePassword(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    cb(err, isMatch);
  });
};

/**
 * Helper method for getting user's gravatar.
 */
userSchema.methods.gravatar = function gravatar(size) {
  if (!size) {
    size = 200;
  }
  if (!this.email) {
    return `https://gravatar.com/avatar/?s=${size}&d=retro`;
  }
  const md5 = crypto.createHash('md5').update(this.email).digest('hex');
  return `https://gravatar.com/avatar/${md5}?s=${size}&d=retro`;
};

/**
 * Helper method for adding role to user.
 */
userSchema.methods.setRole = function setRole(role) {
  //
  const user = this;
  if (role) {
    user.role = {
      _id: role._id,
      name: role.name,
      slug: role.slug,
      description: role.description,
    };
    this.setOccupation(role.name);
  }
};

/**
 * Helper method for describing user occupation.
 */
userSchema.methods.setOccupation = function setRole(occupation) {
  //
  const user = this;
  if (occupation) {
    user.profile.occupation = occupation;
  }
};

/**
 * Helper method for adding address details to user
 * @type {Model}
 */
userSchema.methods.setAddress = function setAddress(address) {
  // Load user
  const user = this;
  // Set addresses details if address is provided
  if (address) {
    user.address.line = address.line;
    user.address.city = address.city;
    user.address.state = address.state;
    user.address.country = address.country;
    user.address.postcode = address.postcode;
    this.setLocation(address);
  }
};
/**
 * Set user profile location based on address provided city
 * @type {Model}
 */
userSchema.methods.setLocation = function setLocation(address) {
  const user = this;
  if (address) {
    user.profile.location = address.city;
  }
};


const User = mongoose.model('User', userSchema);

module.exports = User;
