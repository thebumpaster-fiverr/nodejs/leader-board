const mongoose = require('mongoose');

const publicationSchema = new mongoose.Schema({
  employeeId: String,
  data: {
    title: String,
    color: String,
    productivity: String,
    recommendation: String,
    grade: Number,
    description: String,
    estimate: Number,
    consumed: Number,
  },
  gradedBy: String,
}, { timestamps: true });

/**
 * Data construction based on input of a grade
 */
publicationSchema.pre('save', function save(next) {
  // Time Tracking
  if ((this.data.consumed / this.data.estimate) <= 8) {
    this.data.grade = Number(10);
  } else if ((this.data.consumed / this.data.estimate) > 8 && (this.data.consumed / this.data.estimate) <= 8.5559) {
    this.data.grade = Number(9);
  } else if ((this.data.consumed / this.data.estimate) > 8.5559 && (this.data.consumed / this.data.estimate) <= 9) {
    this.data.grade = Number(8);
  } else if ((this.data.consumed / this.data.estimate) > 9 && (this.data.consumed / this.data.estimate) <= 10) {
    this.data.grade = Number(7);
  } else if ((this.data.consumed / this.data.estimate) > 10 && (this.data.consumed / this.data.estimate) <= 11) {
    this.data.grade = Number(6);
  } else if ((this.data.consumed / this.data.estimate) > 11 && (this.data.consumed / this.data.estimate) <= 12) {
    this.data.grade = Number(5);
  } else if ((this.data.consumed / this.data.estimate) > 12 && (this.data.consumed / this.data.estimate) <= 13) {
    this.data.grade = Number(4);
  } else if ((this.data.consumed / this.data.estimate) > 13 && (this.data.consumed / this.data.estimate) <= 14) {
    this.data.grade = Number(3);
  } else if ((this.data.consumed / this.data.estimate) > 14 && (this.data.consumed / this.data.estimate) <= 15) {
    this.data.grade = Number(2);
  } else {
    this.data.grade = Number(1);
  }
  // Grading
  if (this.data.grade >= 0 && this.data.grade <= 4) {
    this.data.productivity = 'No performance';
    this.data.recommendation = 'Needs Improvment';
    this.data.color = 'black';
  } else if (this.data.grade > 4 && this.data.grade <= 6) {
    this.data.productivity = 'Underperformance';
    this.data.recommendation = 'Needs Improvment';
    this.data.color = 'red';
  } else if (this.data.grade > 6 && this.data.grade <= 8) {
    this.data.productivity = 'Average performance';
    this.data.recommendation = 'Meets expectations';
    this.data.color = 'yellow';
  } else if (this.data.grade > 8 && this.data.grade <= 10) {
    this.data.color = 'green';
    this.data.productivity = 'Optimum performance';
    this.data.recommendation = 'Exceeds Expectations';
  } else {
    this.data.color = 'black';
    this.data.productivity = 'Error';
  }
  next();
});

publicationSchema.methods.preUpdate = function preUpdate(publication) {
  // Time Tracking
  if ((publication.data.consumed / publication.data.estimate) <= 8) {
    this.data.grade = Number(10);
  } else if ((publication.data.consumed / publication.data.estimate) > 8 && (publication.data.consumed / publication.data.estimate) <= 8.5559) {
    this.data.grade = Number(9);
  } else if ((publication.data.consumed / publication.data.estimate) > 8.5559 && (publication.data.consumed / publication.data.estimate) <= 9) {
    this.data.grade = Number(8);
  } else if ((publication.data.consumed / publication.data.estimate) > 9 && (publication.data.consumed / publication.data.estimate) <= 10) {
    this.data.grade = Number(7);
  } else if ((publication.data.consumed / publication.data.estimate) > 10 && (publication.data.consumed / publication.data.estimate) <= 11) {
    this.data.grade = Number(6);
  } else if ((publication.data.consumed / publication.data.estimate) > 11 && (publication.data.consumed / publication.data.estimate) <= 12) {
    this.data.grade = Number(5);
  } else if ((publication.data.consumed / publication.data.estimate) > 12 && (publication.data.consumed / publication.data.estimate) <= 13) {
    this.data.grade = Number(4);
  } else if ((publication.data.consumed / publication.data.estimate) > 13 && (publication.data.consumed / publication.data.estimate) <= 14) {
    this.data.grade = Number(3);
  } else if ((publication.data.consumed / publication.data.estimate) > 14 && (publication.data.consumed / publication.data.estimate) <= 15) {
    this.data.grade = Number(2);
  } else {
    this.data.grade = Number(1);
  }

  if (this.data.grade >= 0 && this.data.grade <= 4) {
    this.data.productivity = 'No performance';
    this.data.recommendation = 'Needs Improvment';
    this.data.color = 'black';
  } else if (this.data.grade > 4 && this.data.grade <= 6) {
    this.data.productivity = 'Underperformance';
    this.data.recommendation = 'Needs Improvment';
    this.data.color = 'red';
  } else if (this.data.grade > 6 && this.data.grade <= 8) {
    this.data.productivity = 'Average performance';
    this.data.recommendation = 'Meets expectations';
    this.data.color = 'yellow';
  } else if (this.data.grade > 8 && this.data.grade <= 10) {
    this.data.color = 'green';
    this.data.productivity = 'Optimum performance';
    this.data.recommendation = 'Exceeds Expectations';
  } else {
    this.data.color = 'black';
    this.data.productivity = 'Error';
  }
};

const Publication = mongoose.model('Publication', publicationSchema);
module.exports = Publication;
