const mongoose = require('mongoose');

const employeeSchema = new mongoose.Schema({
  email: { type: String, unique: true },

  profile: {
    name: String,
    gender: String,
    location: String,
    picture: String,
    occupation: String,
    phone: Number
  },
  address: {
    line: { type: String },
    city: { type: String },
    state: { type: String },
    country: { type: String },
    postcode: { type: String }
  },

  role: Object,
  reviews: Array,
  publications: Array
}, { timestamps: true });

/**
 * Helper method for adding address details to employee
 * @type {Model}
 */
employeeSchema.methods.setAddress = function setAddress(address) {
  // Load employee
  const employee = this;
  // Set addresses details if address is provided
  if (address) {
    employee.address.line = address.line;
    employee.address.city = address.city;
    employee.address.state = address.state;
    employee.address.country = address.country;
    employee.address.postcode = address.postcode;
    this.setLocation(address);
  }
};
/**
 * Set employee profile location based on address provided city
 * @type {Model}
 */
employeeSchema.methods.setLocation = function setLocation(address) {
  const employee = this;
  if (address) {
    employee.profile.location = address.city;
  }
};

// Add, Remove Roles
/**
 * Helper method for adding role to employee.
 */
employeeSchema.methods.setRole = function setRole(role) {
  //
  const employee = this;
  if (role.state) {
    employee.role = {
      state: false,
    };
    employee.setOccupation('Not enrolled');
  } else {
    employee.role = {
      _id: role._id,
      name: role.name,
      slug: role.slug,
      description: role.description,
    };
    employee.setOccupation(role.name);
  }
};
/**
 * Helper method for describing employee occupation.
 */
employeeSchema.methods.setOccupation = function setRole(occupation) {
  //
  const employee = this;
  if (occupation) {
    employee.profile.occupation = occupation;
  }
};
// Add Reviews & Publications
// Get Total Reviews & Publications Stats
// Get Top Review & Publication
// Get Lowest Review & Publication

const Employee = mongoose.model('Employee', employeeSchema);
module.exports = Employee;
