const mongoose = require('mongoose');

const activitySchema = new mongoose.Schema({
  slug: { type: String },
  name: { type: String },
  description: { type: String }

}, { timestamps: true });

const Activity = mongoose.model('Activity', activitySchema);

module.exports = Activity;
