const mongoose = require('mongoose');

const reviewSchema = new mongoose.Schema({
  employeeId: String,
  data: {
    color: String,
    title: String,
    productivity: String,
    grade: Number,
    description: String,
    recommendation: String,
    estimate: Number,
    consumed: Number,
  },
  gradedBy: String,
}, { timestamps: true });

/**
 * Data construction based on input of a grade
 */
reviewSchema.pre('save', function save(next) {
  // Time Tracking
  console.log('save', this);
  if ((this.data.consumed / this.data.estimate) <= 8) {
    this.data.grade = Number(10);
  } else if ((this.data.consumed / this.data.estimate) > 8 && (this.data.consumed / this.data.estimate) <= 8.5559) {
    this.data.grade = Number(9);
  } else if ((this.data.consumed / this.data.estimate) > 8.5559 && (this.data.consumed / this.data.estimate) <= 9) {
    this.data.grade = Number(8);
  } else if ((this.data.consumed / this.data.estimate) > 9 && (this.data.consumed / this.data.estimate) <= 10) {
    this.data.grade = Number(7);
  } else if ((this.data.consumed / this.data.estimate) > 10 && (this.data.consumed / this.data.estimate) <= 11) {
    this.data.grade = Number(6);
  } else if ((this.data.consumed / this.data.estimate) > 11 && (this.data.consumed / this.data.estimate) <= 12) {
    this.data.grade = Number(5);
  } else if ((this.data.consumed / this.data.estimate) > 12 && (this.data.consumed / this.data.estimate) <= 13) {
    this.data.grade = Number(4);
  } else if ((this.data.consumed / this.data.estimate) > 13 && (this.data.consumed / this.data.estimate) <= 14) {
    this.data.grade = Number(3);
  } else if ((this.data.consumed / this.data.estimate) > 14 && (this.data.consumed / this.data.estimate) <= 15) {
    this.data.grade = Number(2);
  } else {
    this.data.grade = Number(1);
  }
  // Grading
  if (this.data.grade >= 0 && this.data.grade <= 4) {
    this.data.productivity = 'No performance';
    this.data.recommendation = 'Needs improvement';
    this.data.color = 'black';
  } else if (this.data.grade > 4 && this.data.grade <= 6) {
    this.data.productivity = 'Underperformance';
    this.data.recommendation = 'Needs improvement';
    this.data.color = 'red';
  } else if (this.data.grade > 6 && this.data.grade <= 8) {
    this.data.productivity = 'Average performance';
    this.data.recommendation = 'Meets expectations';
    this.data.color = 'yellow';
  } else if (this.data.grade > 8 && this.data.grade <= 10) {
    this.data.color = 'green';
    this.data.productivity = 'Optimum performance';
    this.data.recommendation = 'Exceeds expectations';
  }
  next();
});

reviewSchema.methods.preUpdate = function preUpdate(review) {
  // Time Tracking
  console.log(review);
  if ((review.data.consumed / review.data.estimate) <= 8) {
    this.data.grade = Number(10);
  } else if ((review.data.consumed / review.data.estimate) > 8 && (review.data.consumed / review.data.estimate) <= 8.5559) {
    this.data.grade = Number(9);
  } else if ((review.data.consumed / review.data.estimate) > 8.5559 && (review.data.consumed / review.data.estimate) <= 9) {
    this.data.grade = Number(8);
  } else if ((review.data.consumed / review.data.estimate) > 9 && (review.data.consumed / review.data.estimate) <= 10) {
    this.data.grade = Number(7);
  } else if ((review.data.consumed / review.data.estimate) > 10 && (review.data.consumed / review.data.estimate) <= 11) {
    this.data.grade = Number(6);
  } else if ((review.data.consumed / review.data.estimate) > 11 && (review.data.consumed / review.data.estimate) <= 12) {
    this.data.grade = Number(5);
  } else if ((review.data.consumed / review.data.estimate) > 12 && (review.data.consumed / review.data.estimate) <= 13) {
    this.data.grade = Number(4);
  } else if ((review.data.consumed / review.data.estimate) > 13 && (review.data.consumed / review.data.estimate) <= 14) {
    this.data.grade = Number(3);
  } else if ((review.data.consumed / review.data.estimate) > 14 && (review.data.consumed / review.data.estimate) <= 15) {
    this.data.grade = Number(2);
  } else {
    this.data.grade = Number(1);
  }
  if (review.data.grade >= 0 && review.data.grade <= 4) {
    this.data.productivity = 'No performance';
    this.data.recommendation = 'Needs Improvment';
    this.data.color = 'black';
  } else if (review.data.grade > 4 && review.data.grade <= 6) {
    this.data.productivity = 'Underperformance';
    this.data.recommendation = 'Needs Improvment';
    this.data.color = 'red';
  } else if (review.data.grade > 6 && review.data.grade <= 8) {
    this.data.productivity = 'Average performance';
    this.data.recommendation = 'Meets expectations';
    this.data.color = 'yellow';
  } else if (review.data.grade > 8 && review.data.grade <= 10) {
    this.data.color = 'green';
    this.data.productivity = 'Optimum performance';
    this.data.recommendation = 'Exceeds Expectations';
  } else {
    this.data.color = 'black';
    this.data.productivity = 'Error';
  }
};

const Review = mongoose.model('Review', reviewSchema);
module.exports = Review;
