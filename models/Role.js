const mongoose = require('mongoose');

const roleSchema = new mongoose.Schema({
  id: { type: String, unique: true },
  slug: { type: String },
  name: { type: String },
  description: { type: String }

}, { timestamps: true });


const Role = mongoose.model('Role', roleSchema);

module.exports = Role;
