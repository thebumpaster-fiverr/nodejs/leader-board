const User = require('../models/User');
const Activity = require('../models/Activity');
/**
 * GET /login
 * Login page.
 */
exports.getAdmin = (req, res, next) => {
  if (!req.user) {
    return res.redirect('/login');
  }
  User.find({}, (err, users) => {
    if (err) { return next(err); }
    if (users.length < 1) {
      req.flash('error', { msg: 'Fill data in your account first' });
      res.redirect('/account');
    }
    res.render('admin/index', {
      title: 'List of Administrators',
      admins: users
    });
  });
};

/**
 * GET /admin/new
 * Create a new employee page.
 */
exports.getAdminNew = (req, res) => {
  if (!req.user) {
    return res.redirect('/administration');
  }
  res.render('admin/new', {
    title: 'New Administrator'
  });
};

/**
 * POST /admin/new
 * Dashboard page.
 */
exports.postAdmin = (req, res, next) => {
  if (!req.user) {
    return res.redirect('/administration');
  }
  req.assert('adminName', 'Name cannot be blank').notEmpty();
  req.assert('adminEmail', 'Email cannot be blank').notEmpty();
  req.assert('adminPassword', 'Password cannot be blank').notEmpty();
  req.assert('adminGender', 'Gender cannot be blank').notEmpty();
  req.assert('adminPhoneNumber', 'Phone cannot be blank').notEmpty();
  req.assert('adminCity', 'City cannot be blank').notEmpty();

  const errors = req.validationErrors();
  if (errors) {
    req.flash('errors', errors);
    res.redirect('back');
  }
  const user = new User({
    email: req.body.adminEmail,
    password: req.body.adminPassword
  });
  user.profile.name = req.body.adminName || '';
  user.profile.gender = req.body.adminGender || '';
  user.profile.location = req.body.location || '';
  user.profile.website = req.body.adminWebsite || '';
  user.profile.occupation = req.body.adminOccupation || '';
  user.profile.phone = req.body.adminPhoneNumber || '';
  user.profile.company = req.body.adminCompanyName || '';
  user.facebook = req.body.adminFacebook || '';
  user.facebook = req.body.adminFacebook || '';
  user.facebook = req.body.adminFacebook || '';
  user.twitter = req.body.adminTwitter || '';
  user.google = req.body.adminGoogle || '';
  user.github = req.body.adminGithub || '';
  user.instagram = req.body.adminInstagram || '';
  user.linkedin = req.body.adminLinkedin || '';
  user.steam = req.body.adminSteam || '';
  // Check if address details have been passed
  if (
    req.body.adminAddress
    || req.body.adminCity
    || req.body.adminState
    || req.body.adminCountry
    || req.body.adminPostcode
  ) {
    const address = {
      line: req.body.adminAddress || '',
      city: req.body.adminCity || '',
      state: req.body.adminState || '',
      postcode: req.body.adminCountry || '',
      country: req.body.adminPostcode || '',
    };
    user.setAddress(address);
  }
  user.save((err) => {
    if (err) {
      if (err.code === 11000) {
        req.flash('errors', { msg: 'The email address you have entered is already associated with an account.' });
        return res.redirect('back');
      }
      return next(err);
    }
    const activity = new Activity({
      name: user.email,
      slug: 'account-create',
      description: `User ${user.email} has updated his account on ${Date()}`
    });
    activity.save((err) => {
      if (err) { return next(err); }
      req.flash('success', { msg: 'Profile information has been updated.' });
      res.redirect('/administration');
    });
  });
};

/**
 * GET /admin/:_id/delete
 * Delete admin document.
 */
exports.AdminDelete = (req, res, next) => {
  if (!req.user && !req.params.adminId) {
    return res.redirect('/administration');
  }
  // Find and delete employee account
  User.findOneAndDelete({ _id: req.params.adminId }, (err, user) => {
    if (err) { return next(err); }
    const activity = new Activity({
      name: req.user.email,
      slug: 'user-delete',
      description: `User ${req.user.email} has deleted an user account ${user.email} on ${Date()}`
    });
    activity.save((err) => {
      if (err) { return next(err); }
      req.flash('info', { msg: 'User has been deleted.' });
      res.redirect('/administration');
    });
  });
};
