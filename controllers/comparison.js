const Employee = require('../models/Employee');
const Review = require('../models/Review');
const reviewController = require('./review');

exports.getComparison = (req, res, next) => {
  if (!req.user) {
    return res.redirect('/');
  }
  Employee.find({}, (err, employees) => {
    if (err) {
      return next(err);
    }
    Review.find({}, (err, reviews) => {
      if (err) { return next(err); }
      if (employees.length < 2) {
        res.render('dashboard/comparison', {
          title: 'Comparison Mode',
          state: false
        });
      }
      const employeeRevs = [];
      for (let i = 0; i < employees.length; i++) {
        if (employees[i].reviews.length > 0) {
          console.log('found reviews');
          const revMatch = reviewController.reviewPerformerSerializer(employees[i].reviews, reviews);
          console.log('revMatch: ', revMatch);
          employeeRevs.push({
            [employees[i]._id]: revMatch
          });
        }
      }
      console.log(employeeRevs);
      if (employeeRevs.length > 0) {
        // Load all employees and their roles, let them be visible on both employee pickers
        res.render('dashboard/comparison', {
          title: 'Comparison Mode',
          employees,
          employeesJs: {
            employees: JSON.stringify(employees).replace(/<\//g, '<\\/'),
            employeesRevs: JSON.stringify(employeeRevs),
          },
          employee: {
            employeeRevs,
          }
        });
      } else {
        // Load no data information
        res.render('dashboard/comparison', {
          title: 'Comparison Mode',
          employees,
          employeesJs: {
            employees: JSON.stringify(employees).replace(/<\//g, '<\\/'),
            employeesRevs: false,
          },
          employee: {
            employeeRevs: false,
          }
        });
      }
    });
  });
};

exports.postComparison = (req, res, next) => {
  if (!req.user) {
    return res.redirect('/');
  }
  // Load two group of users
  // Find all publications and reviews for each user and calculate average
  // Create chart from reviews on all users, divided by grouping colors
  // Create chart from publications on all users, divided by grouping colors
  // Prepare general stats
};
