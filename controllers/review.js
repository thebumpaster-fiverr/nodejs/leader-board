
exports.reviewPerformerSerializer = (listOfPublicationsIds, publications) => {
  function findInArray(target, toMatch) {
    let found; let i; let j; let cur;

    found = false;
    const targetMap = {};
    const element = [];

    // Put all values in the `target` array into a map, where
    //  the keys are the values from the array
    for (i = 0, j = target.length; i < j; i++) {
      cur = target[i];
      targetMap[cur] = true;
    }

    // Loop over all items in the `toMatch` array and see if any of
    //  their values are in the map from before
    for (i = 0, j = toMatch.length; !found && (i < j); i++) {
      cur = toMatch[i];
      found = !!targetMap[cur];
      if (found) {
        console.log(cur);
      }
      // If found, `targetMap[cur]` will return true, otherwise it
      //  will return `undefined`...that's what the `!!` is for
    }
    return element;
  }

  const result = [];

  for (let k = 0; k < listOfPublicationsIds.length; k++) {
    for (let p = 0; p < publications.length; p++) {
      if (listOfPublicationsIds[k].equals(publications[p]._id)) {
        result.push(publications[p]);
      }
    }
  }
  console.log('result ', result);

  return result;
};

exports.reviewsSerializer = (activities) => {
  if (activities.length < 1) {
    return 0;
  }
  function pad(n) {
    return n.toString().length === 1 ? `0${n}` : n;
  }

  const array = [];
  for (let i = 0; i < activities.length; i++) {
    const thisDate = activities[i].createdAt;
    const day = pad(thisDate.getDate());
    const month = pad(thisDate.getMonth() + 1);
    const key = [month, day].join('-');
    array[i] = {
      date: key,
      grade: activities[i].data.grade,
      recommendation: activities[i].data.recommendation,
      productivity: activities[i].data.productivity,
      color: activities[i].data.color,
    };
  }
  return array;
};

exports.reviewsChartSerializer = (activities) => {
  if (activities.length < 1) {
    return 0;
  }
  function pad(n) {
    return n.toString().length === 1 ? `0${n}` : n;
  }

  const obj = {};
  const result = {
    date: [],
    count: []
  };
  for (let i = 0; i < activities.length; i++) {
    const thisDate = activities[i].createdAt;
    const day = pad(thisDate.getDate());
    const month = pad(thisDate.getMonth() + 1);
    const year = thisDate.getFullYear();
    const key = [year, day, month].join('-');
    obj[key] = obj[key] || 0;
    obj[key]++;
  }
  let counter = 0;
  Object.keys(obj).forEach((key) => {
    result.date[counter] = key;
    result.count[counter] = obj[key];
    counter++;
  });

  return result;
};

exports.reviewsDataListSerializer = (activities) => {
  if (activities.length < 1) {
    return 0;
  }
  const obj = {
    gradeSum: [],
    count: []
  };
  const result = [];

  for (let i = 0; i < activities.length; i++) {
    const key = activities[i].data.productivity;
    obj.gradeSum[key] = obj.gradeSum[key] || 0;
    obj.gradeSum[key] += activities[i].data.grade;
    obj.count[key] = obj.count[key] || 0;
    obj.count[key]++;
  }
  let cou = 0;
  Object.keys(obj.count).forEach((key) => {
    result[cou] = { performance: key, average: (obj.gradeSum[key] / obj.count[key]).toFixed(1) };
    cou++;
  });

  return result;
};
