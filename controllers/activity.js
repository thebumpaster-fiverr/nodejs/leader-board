const Activity = require('../models/Activity');
const User = require('../models/User');

/**
 * GET /activity
 * Login page.
 */
exports.getActivityLog = (req, res, next) => {
  Activity.find({}, (err, activityData) => {
    if (err) { return next(err); }
    if (activityData) {
      const result = [];
      for (let i = 0; i < activityData.length; i++) {
        User.findOne({ email: activityData[i].name }, (err, user) => {
          if (err) { return next(err); }
          result.push({
            _id: activityData[i]._id,
            name: activityData[i].name,
            slug: activityData[i].slug,
            description: activityData[i].description,
            createdAt: activityData[i].createdAt,
            user
          });
        });
      }
      res.render('activity/index', {
        title: 'Activity Log',
        activities: result
      });
    } else {
      res.render('activity/index', {
        title: 'Activity Log',
        activities: false
      });
    }
  });
};

/**
 * POST /activity/new
 * Report activity log request for extra tweaks.
 */
exports.postActivity = (req, res, next) => {
  if (req.user) {
    return res.redirect('/');
  }
  req.assert('slug', 'Slug is not valid').notEmpty();
  req.assert('name', 'Name cannot be blank').notEmpty();
  req.assert('description', 'Description cannot be blank').notEmpty();

  const errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('back');
  }
  const activity = new Activity({
    name: req.body.name,
    slug: req.body.slug,
    description: req.body.description
  });

  activity.save((err) => {
    if (err) {
      return next(err);
    }
    res.redirect('/activity');
  });
};

exports.activitySerializer = (activities) => {
  if (activities.length < 1) {
    return 0;
  }
  function pad(n) {
    return n.toString().length === 1 ? `0${n}` : n;
  }

  const array = [];
  for (let i = 0; i < activities.length; i++) {
    const thisDate = activities[i].createdAt;
    const day = pad(thisDate.getDate());
    const month = pad(thisDate.getMonth() + 1);
    const year = thisDate.getFullYear();
    const key = [year, day, month].join('-');
    array[i] = {
      date: key,
      slug: activities[i].slug
    };
  }
  return array;
};

exports.activityChartSerializer = (activities) => {
  if (activities.length < 1) {
    return 0;
  }
  function pad(n) {
    return n.toString().length === 1 ? `0${n}` : n;
  }

  const obj = {};
  const result = {
    date: [],
    count: []
  };
  for (let i = 0; i < activities.length; i++) {
    const thisDate = activities[i].createdAt;
    const day = pad(thisDate.getDate());
    const month = pad(thisDate.getMonth() + 1);
    const year = thisDate.getFullYear();
    const key = [year, day, month].join('-');
    obj[key] = obj[key] || 0;
    obj[key]++;
  }
  let counter = 0;
  Object.keys(obj).forEach((key) => {
    result.date[counter] = key;
    result.count[counter] = obj[key];
    counter++;
  });

  return result;
};
