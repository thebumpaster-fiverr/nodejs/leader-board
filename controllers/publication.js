const moment = require('moment');

exports.publicationSerializer = (listOfPublicationsIds, publications) => {
  function findInArray(target, toMatch) {
    let found; let i; let j; let cur;

    found = false;
    const targetMap = {};
    const element = [];

    // Put all values in the `target` array into a map, where
    //  the keys are the values from the array
    for (i = 0, j = target.length; i < j; i++) {
      cur = target[i];
      targetMap[cur] = true;
    }

    // Loop over all items in the `toMatch` array and see if any of
    //  their values are in the map from before
    for (i = 0, j = toMatch.length; !found && (i < j); i++) {
      cur = toMatch[i];
      found = !!targetMap[cur];
      if (found) {
        element.push(toMatch[i]);
      }
      // If found, `targetMap[cur]` will return true, otherwise it
      //  will return `undefined`...that's what the `!!` is for
    }
    return element;
  }
  const lookup = [];
  const result = [];
  for (let j = 0; j < publications.length; j++) {
    lookup.push(publications[j]._id);
  }
  const keys = findInArray(listOfPublicationsIds, lookup);

  for (let j = 0; j < publications.length; j++) {
    for (let i = 0; i < keys.length; i++) {
      if (publications[j]._id === keys[i] ) {
        result.push(publications[j]);
      }
    }
  }
  return result;
};

exports.serializeQuarts = (publications) => {
  const obj = {
    first: [],
    second: [],
    third: [],
    fourth: []
  };
  for (let i = 0; i < publications.length; i++) {
    const publicationDate = publications[i].createdAt;
    const thisDate = Date.now();
    const difference = moment(publicationDate).diff(moment(thisDate), 'months', true);
    if (difference <= 3) {
      obj.first.push(publications[i]);
    } else if (difference <= 6) {
      obj.second.push(publications[i]);
    } else if (difference <= 9) {
      obj.third.push(publications[i]);
    } else if (difference <= 12) {
      obj.fourth.push(publications[i]);
    }
  }
  return obj;
};

exports.publicationPieCharts = (publications) => {

  let count1 = 0;
  let grade1 = 0;
  let count2 = 0;
  let grade2 = 0;
  let count3 = 0;
  let grade3 = 0;
  let count4 = 0;
  let grade4 = 0;
  let i = 0;

  for (i; i < publications.length; i++) {
    if (publications[i].data.productivity === 'No performance') {
      count4++;
      grade4 += publications[i].data.grade;
    } else if (publications[i].data.productivity === 'Underperformance') {
      count3++;
      grade3 += publications[i].data.grade;
    } else if (publications[i].data.productivity === 'Average performance') {
      count1++;
      grade1 += publications[i].data.grade;
    } else if (publications[i].data.productivity === 'Optimum performance') {
      count2++;
      grade2 += publications[i].data.grade;
    }
  }
  let value1 = Math.floor((count1 / i) * 100);
  if (isNaN(value1)) {
    value1 = 0;
  }
  let value2 = Math.floor((count2 / i) * 100);
  if (isNaN(value2)) {
    value2 = 0;
  }
  let value3 = Math.floor((count3 / i) * 100);
  if (isNaN(value3)) {
    value3 = 0;
  }
  let value4 = Math.floor((count4 / i) * 100);
  if (isNaN(value4)) {
    value4 = 0;
  }

  if (count1 === 0 || i === 0) {
    value1 = 0;
  } else if (count2 === 0 || i === 0) {
    value2 = 0;
  } else if (count3 === 0 || i === 0) {
    value3 = 0;
  } else if (count4 === 0 || i === 0) {
    value4 = 0;
  }

  const arr = [
    {
      value: value1,
      className: 'custom',
      meta: {
        color: '#4CAF50'
      }
    },
    {
      value: value2,
      className: 'custom',
      meta: {
        color: '#FFEB3B'
      }
    },
    {
      value: value3,
      className: 'custom',
      meta: {
        color: '#F44336'
      }
    },
    {
      value: value4,
      className: 'custom',
      meta: {
        color: '#716aca'
      }
    }
  ];
  return arr;
};
