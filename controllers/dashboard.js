const Employee = require('../models/Employee');
const Activity = require('../models/Activity');
const activityController = require('../controllers/activity');
const reviewController = require('../controllers/review');
const publicationController = require('../controllers/publication');
const Review = require('../models/Review');
const Publication = require('../models/Publication');


/*
 * Get Dashboard
 */
exports.getDashboard = (req, res, next) => {
  if (!req.user) {
    res.redirect('/dashboard');
  }
  // Activity
  // const startDateActivity = moment(Date.now());
  // startDateActivity.subtract(10, 'week');
  // const endDateActivity = moment(Date.now());

  Activity.find({
  }, (err, activities) => {
    if (err) { return next(err); }
    Employee.find({}, (err, employees) => {
      if (err) { return next(err); }
      Review.find({}, (err, reviews) => {
        Publication.find({}, (err, publications) => {
          if (err) { return next(err); }
          const topPerformers = [];
          for (let e = 0; e < employees.length; e++) {
            const performer = {};
            if (employees[e].publications.length > 0 || employees[e].reviews.length > 0) {
              const employeePubs = publicationController.publicationSerializer(employees[e].publications, publications);
              const employeeRevs = reviewController.reviewPerformerSerializer(employees[e].reviews, reviews);
              if (employeePubs.length > 0) {
                let pubSum = 0;
                for (let ep = 0; ep < employeePubs.length; ep++) {
                  pubSum += Number(employeePubs[ep].data.grade);
                }
                performer.PubAverage = pubSum / employeePubs.length || 0;
                performer.PubAverage = performer.PubAverage.toFixed(1);
              } else {
                performer.PubAverage = 0;
              }
              if (employeeRevs.length > 0) {
                let revSum = Number(0);
                for (let er = 0; er < employeeRevs.length; er++) {
                  revSum += Number(employeeRevs[er].data.grade);
                }
                performer.reviewAverage = revSum / employeeRevs.length || 0;
                performer.reviewAverage = performer.reviewAverage.toFixed(1);
              } else {
                performer.reviewAverage = 0;
              }
              performer.averageSum = Number(performer.reviewAverage) + Number(performer.PubAverage) || 0;
              performer.average = performer.averageSum / 2;
              performer.average.toFixed(3);
            }
            performer.name = employees[e].profile.name;
            performer.email = employees[e].email;
            topPerformers.push(performer);
          }
          topPerformers.sort((a, b) => {
            return b.average - a.average;
          });
          console.log(topPerformers);
          const quartedPublications = publicationController.serializeQuarts(publications);
          const stats = {};
          stats.first = publicationController.publicationPieCharts(quartedPublications.first);
          stats.second = publicationController.publicationPieCharts(quartedPublications.second);
          stats.third = publicationController.publicationPieCharts(quartedPublications.third);
          stats.fourth = publicationController.publicationPieCharts(quartedPublications.fourth);
          const publicationData = {
            piechart: JSON.stringify(stats),
            stats
          };
          const reviewsData = {
            reviews: reviewController.reviewsSerializer(reviews),
            chart: JSON.stringify(reviewController.reviewsChartSerializer(reviews)),
            list: reviewController.reviewsDataListSerializer(reviews)
          };
          const chartData = activityController.activityChartSerializer(activities);

          const activityData = {
            stats: {
              publishedReviews: 0,
              updatedAccounts: 0,
              loginCount: 0,
            },
            activity: activityController.activitySerializer(activities),
            chart: JSON.stringify(chartData)
          };

          // Select up to 4 slugs get total count, for chart get last 10 weeks of activity
          for (let i = 0; i < activities.length; i++) {
            if (activities[i].slug === 'review-create' || activities[i].slug === 'employee-add-publication') {
              activityData.stats.publishedReviews += 1;
            } else if (activities[i].slug === 'employee-update') {
              activityData.stats.updatedAccounts += 1;
            } else if (activities[i].slug === 'account-login') {
              activityData.stats.loginCount += 1;
            }
          }
          activityData.stats.employeeCount = employees.length;

          res.render('dashboard/index', {
            title: 'Dashboard',
            user: req.user,
            activityData,
            reviewsData,
            publicationData,
            publicationCount: publications.length,
            topPerformers
          });
        });
      });
    });
  });
};


// exports.getActivityByDateRange = (req, res, next) => {
//   if (!req.user) {
//     res.redirect('/dashboard');
//   }
//   Activity.find({}, (err, activities) => {
//     if (err) { return next(err); }
//     if (activities.length < 1) {
//       return false;
//     }
//     console.log(activities);
//     req.flash('data', { activities });
//     res.redirect('/dashboard');
//   });
// };
//
// exports.getActivityBySlug = async (req, res, next) => {
//   try {
//     if (!req.user) {
//       res.redirect('/dashboard');
//     }
//     Activity.find({
//       slug: req.body.slug
//     }, (err, activities) => {
//       if (err) { return next(err); }
//       if (activities.length < 1) {
//         res.json({
//           status: false
//         });
//       }
//       console.log(activities);
//       req.flash('data', { activities });
//       res.json(activities);
//     });
//   } catch (e) {
//     return e;
//   }
// };
//
// exports.getEmployeesCount = (req, res, next) => {
//   Employee.find({}, (err, employees) => {
//     if (err) { return next(err); }
//     res.json({
//       employeesCount: employees.length || 0
//     });
//   });
// };
//
// exports.getReviewsByDateRange = async (req, res, next) => {
//   try {
//     Review.find({
//       updatedAt: {
//         $qt: req.body.startDate,
//         $lt: req.body.endDate
//       }
//     }, (err, reviews) => {
//       if (err) { return next(err); }
//       res.json({
//         reviews: reviews || 0
//       });
//     });
//   } catch (e) {
//     return e;
//   }
// };


// exports.postDashboardData = (req, res, next) => {
//   if (!req.user) {
//     res.redirect('/dashboard');
//   }
//   // Reviews
//   function getReviewsByDateRange(startDate, endDate) {
//     Review.find({
//       updatedAt: {
//         $qt: startDate,
//         $lt: endDate
//       }
//     }, (err, reviews) => {
//       if (err) { return err; }
//       if (reviews.length < 1) {
//         return false;
//       }
//       return reviews;
//     });
//   }
//
//   // Catch last 4 months of reviews general stats
//   const startDateReview = moment(Date.now());
//   startDateReview.subtract(4, 'months');
//   const reviews = getReviewsByDateRange(startDateReview, Date.now());
//
//   // Activity
//   let activity;
//   const startDateActivity = moment(Date.now());
//   startDateActivity.subtract(10, 'week');
//   const endDateActivity = moment(Date.now());
//   Activity.find({
//     updatedAt: {
//       $gt: startDateActivity,
//       $lt: endDateActivity
//     }
//   }, (err, activities) => {
//     if (err) { return next(err); }
//     if (activities.length < 1) {
//       activity = false;
//     }
//     // Set activites
//     activity = activities;
//     console.log(activity);
//
//     let publishedReviews = 0;
//     let updatedAccounts = 0;
//     let loginCount = 0;
//     let newEmployees = 0;
//
//     // Select up to 4 slugs get total count, for chart  get last 10 weeks of activity and summarize
//     for (let i = 0; i <= activity.length; i++) {
//       if (activity[i].slug === 'review-added') {
//         publishedReviews += 1;
//       } else if (activity[i].slug === 'employee-update') {
//         updatedAccounts += 1;
//       } else if (activity[i].slug === 'account-login') {
//         loginCount += 1;
//       }
//     }
//
//     Employee.find({}, (err, employees) => {
//       if (err) { return next(err); }
//       if (employees.length > 1) {
//         newEmployees = employees.length;
//         // Publications
//         // Get a stats for general input, quart's stats and inclusively all publications
//
//         // Best Performers
//         // Get top 10 employees by reviews, last month, last year and all time
//
//         const activityData = {
//           activity,
//           publishedReviews,
//           newEmployees,
//           updatedAccounts,
//           loginCount
//         };
//         req.flash('data', { activityData });
//         res.redirect('/dashboard');
//       }
//     });
//   });
// };
