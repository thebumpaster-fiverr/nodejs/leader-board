const moment = require('moment');
const Employee = require('../models/Employee');
const Activity = require('../models/Activity');
const Role = require('../models/Role');
const User = require('../models/User');
const Review = require('../models/Review');
const Publication = require('../models/Publication');

// Index
/**
 * GET /employees
 * List of employees page.
 */
exports.getEmployeeList = (req, res, next) => {
  if (!req.user) {
    return res.redirect('/employees');
  }
  Employee.find({}, (err, users) => {
    if (err) { return next(err); }
    if (users.length < 1) {
      res.render('employee/index', {
        title: 'List of Employees',
        employees: false
      });
    } else {
      console.log(users);
      res.render('employee/index', {
        title: 'List of Employees',
        employees: users
      });
    }
  });
};

/**
 * GET /employee/roles
 * Get employee role list and tools.
 */
exports.getEmployeeRoles = (req, res, next) => {
  if (!req.user) {
    return res.redirect('/employees');
  }
  Role.find().lean().exec((err, rolesList) => {
    let rolesData = '';
    if (err) { return next(err); }
    if (rolesList.length === 0) {
      rolesData = 'There are no roles, please create one';
    } else {
      rolesData = rolesList;
    }
    res.render('employee/roles', {
      title: 'Employee Roles',
      roles: rolesData
    });
  });
};

/**
 *  GET /employee/:_id/reviews
 *  Show all reviews of one employee
 */
exports.getEmployeeReviewList = (req, res, next) => {
  if (!req.user && !req.params.employeeId) {
    return res.redirect('/employees');
  }
  Employee.findOne({ _id: req.params.employeeId }, (err, employee) => {
    if (err) {
      return next(err);
    }
    if (!employee) {
      res.render('employee/reviews/index', {
        title: 'Employee Reviews',
        user: req.user,
        employee: false
      });
    } else {
      Review.find({ employeeId: employee._id }, (err, reviews) => {
        if (err) { return next(err); }
        if (reviews.length < 1) {
          res.render('employee/reviews/index', {
            title: 'Employee Reviews',
            user: req.user,
            employee,
            reviews: false
          });
        } else {
          res.render('employee/reviews/index', {
            title: 'Employee Reviews',
            user: req.user,
            employee,
            reviews
          });
        }
      });
    }
  });
};

/**
 *  GET /employee/:_id/publications
 *  Show employee publications list
 */
exports.getEmployeePublicationList = (req, res, next) => {
  if (!req.user && !req.params.employeeId) {
    return res.redirect('/employees');
  }
  Employee.findOne({ _id: req.params.employeeId }, (err, employee) => {
    if (err) {
      return next(err);
    }
    if (!employee) {
      res.render('employee/publications/index', {
        title: 'Employee Publications',
        user: req.user,
        employee: false
      });
    } else {
      Publication.find({ employeeId: employee._id }, (err, publications) => {
        if (err) { return next(err); }
        if (publications.length < 1) {
          res.render('employee/publications/index', {
            title: 'Employee Publications',
            user: req.user,
            employee,
            publications: false
          });
        } else {
          res.render('employee/publications/index', {
            title: 'Employee Publications',
            user: req.user,
            employee,
            publications
          });
        }
      });
    }
  });
};


// Details
/**
 * GET /employee/:_id
 * List of employees page.
 */
exports.getEmployeeById = (req, res, next) => {
  if (!req.user && !req.params.employeeId) {
    return res.redirect('/employees');
  }
  Employee.findOne({ _id: req.params.employeeId }, (err, employee) => {
    if (err) {
      return next(err);
    }
    if (!employee) {
      req.flash('error', { msg: 'There is no employee with this id. Check your url and try again.' });
      res.redirect('back');
    } else {
      Role.find({}, (err, roles) => {
        if (err) { return next(err); }
        if (roles.length < 1) {
          req.flash('error', { msg: 'There are no roles added yet, please add one for start.' });
          res.redirect('back');
        } else {
          Publication.find({ employeeId: req.params.employeeId }, (err, publications) => {
            if (err) { return next(err); }
            Review.find({ employeeId: req.params.employeeId }, (err, reviews) => {
              if (err) { return next(err); }
              function pad(n) {
                return n.toString().length === 1 ? `0${n}` : n;
              }
              function serializeArray(arr) {
                const array = [];
                for (let i = 0; i < arr.length; i++) {
                  const thisDate = arr[i].createdAt;
                  const day = pad(thisDate.getDate());
                  const month = pad(thisDate.getMonth() + 1);
                  const year = thisDate.getFullYear();
                  const key = [year, day, month].join('-');
                  array[i] = {
                    date: key,
                    grade: arr[i].data.grade,
                    recommendation: arr[i].data.recommendation,
                    color: arr[i].data.color
                  };
                }
                return array;
              }
              const chartData = serializeArray(publications);
              const reviewData = {
                reviews,
                chart: JSON.stringify(serializeArray(reviews)).replace(/<\//g, '<\\/'),
                length: serializeArray(reviews).length
              };
              res.render('employee/details', {
                title: 'Employee Details',
                user: req.user,
                roles,
                employee,
                publications,
                chartData: {
                  chart: JSON.stringify(chartData).replace(/<\//g, '<\\/'),
                  length: chartData.length
                },
                reviewData
              });
            });
          });
        }
      });
    }
  });
};

/**
 *  GET /employee/:_id/review/:_review
 *  Show employee review details
 */
exports.getEmployeeReview = (req, res, next) => {
  if (!req.user && !req.params.employeeId && !req.params.reviewId) {
    return res.redirect('/employees');
  }
  Employee.findOne({ _id: req.params.employeeId }, (err, employee) => {
    if (err) {
      return next(err);
    }
    if (!employee) {
      res.render('employee/reviews/details', {
        title: 'Employee Review Details',
        user: req.user,
        employee: false
      });
    } else {
      Review.findOne({ _id: req.params.reviewId }, (err, review) => {
        if (err) { return next(err); }
        if (!review) {
          res.render('employee/reviews/details', {
            title: 'Employee Review Details',
            user: req.user,
            employee,
            review: false
          });
        } else {
          res.render('employee/reviews/details', {
            title: 'Employee Review Details',
            user: req.user,
            employee,
            review,
            moment
          });
        }
      });
    }
  });
};

/**
 *  GET /employee/:_id/publication/:_publication
 *  Show employee publication details
 */
exports.getEmployeePublication = (req, res, next) => {
  if (!req.user && !req.params.employeeId && !req.params.publicationId) {
    return res.redirect('/employees');
  }
  Employee.findOne({ _id: req.params.employeeId }, (err, employee) => {
    if (err) {
      return next(err);
    }
    if (!employee) {
      res.render('employee/publication/details', {
        title: 'Employee Publication Details',
        user: req.user,
        employee: false
      });
    } else {
      Publication.findOne({ _id: req.params.publicationId }, (err, publication) => {
        if (err) { return next(err); }
        if (!publication) {
          res.render('employee/publication/details', {
            title: 'Employee Publication Details',
            user: req.user,
            employee,
            publication: false
          });
        } else {
          User.findOne({ _id: publication.gradedBy }, (err, graded) => {
            if (err) { return next(err); }
            res.render('employee/publications/details', {
              title: 'Employee Publication Details',
              user: req.user,
              employee,
              publication,
              graded,
              moment
            });
          });
        }
      });
    }
  });
};

/**
 * GET employee/role/:role_id/details
 * List of roles page.
 */
exports.getRoleById = (req, res, next) => {
  if (!req.user && !req.params.roleId) {
    return res.redirect('/employee/roles');
  }
  Role.findOne({ _id: req.params.roleId }).lean().exec((err, role) => {
    if (err) {
      return next(err);
    }
    if (!role) {
      res.render('employee/role/details', {
        title: 'Role Details',
        user: req.user,
        role: false
      });
    } else {
      res.render('employee/role/details', {
        title: 'Role Details',
        user: req.user,
        role
      });
    }
  });
};


// CRUD
/**
 * GET /employee/new
 * Create a new employee page.
 */
exports.getEmployeeNew = (req, res, next) => {
  if (!req.user) {
    return res.redirect('/employees');
  }
  Role.find().lean().exec((err, rolesList) => {
    let rolesData = '';
    if (err) {
      return next(err);
    }
    if (rolesList.length === 0) {
      rolesData = 'There are no roles, please create one';
    } else {
      rolesData = rolesList;
    }
    res.render('employee/new', {
      title: 'New Employee',
      roles: rolesData
    });
  });
};

/**
 * GET /employee/role/new
 * Create a new role page.
 */
exports.getRoleNew = (req, res) => {
  if (!req.user) {
    return res.redirect('/employee/roles');
  }
  res.render('employee/role/new', {
    title: 'New Role'
  });
};

/**
 *  GET /employee/review/new
 *  Show employee new review page
 */
exports.getEmployeeReviewNew = (req, res, next) => {
  if (!req.user) {
    return res.redirect('/employees');
  }
  Employee.find({}, (err, employees) => {
    const date = Date(Date.now());
    if (err) {
      return next(err);
    }
    if (employees.length < 1) {
      res.render('employee/reviews/new', {
        title: 'New Review',
        user: req.user,
        employees: false,
        date
      });
    } else {
      res.render('employee/reviews/new', {
        title: 'New Review',
        user: req.user,
        employees,
        reviews: false,
        date
      });
    }
  });
};

/**
 *  GET /employee/publication/new
 *  Show employee new publication page
 */
exports.getEmployeePublicationNew = (req, res, next) => {
  if (!req.user) {
    return res.redirect('/employees');
  }
  Employee.find({}, (err, employees) => {
    if (err) {
      return next(err);
    }
    const date = Date(Date.now());
    if (!employees) {
      res.render('employee/publications/new', {
        title: 'New Publication',
        user: req.user,
        employees: false,
        date
      });
    } else {
      res.render('employee/publications/new', {
        title: 'New Publication ',
        user: req.user,
        employees,
        publication: false,
        date
      });
    }
  });
};

/**
 *  GET /employee/:_id/review/:_review/update
 *  Show employee review update form
 */
exports.getEmployeeReviewUpdate = (req, res, next) => {
  if (!req.user && !req.params.employeeId && !req.params.reviewId) {
    return res.redirect('/employees');
  }
  Employee.findOne({ _id: req.params.employeeId }, (err, employee) => {
    if (err) {
      return next(err);
    }
    if (!employee) {
      res.render('employee/reviews/update', {
        title: 'Employee Review Update',
        user: req.user,
        employee: false
      });
    } else {
      Review.findOne({ _id: req.params.reviewId }, (err, review) => {
        if (err) { return next(err); }
        if (!review) {
          res.render('employee/reviews/update', {
            title: 'Employee Review Details',
            user: req.user,
            employee,
            review: false
          });
        } else {
          const reviewJson = JSON.stringify(review).replace(/<\//g, '<\\/');
          res.render('employee/reviews/update', {
            title: 'Employee Review Details',
            user: req.user,
            employee,
            review,
            reviewJson,
            moment
          });
        }
      });
    }
  });
};


/**
 *  GET /employee/:_id/publication/:_publication
 *  Show employee publication details
 */
exports.getEmployeePublicationUpdate = (req, res, next) => {
  if (!req.user && !req.params.employeeId && !req.params.publicationId) {
    res.redirect('/employees');
  }
  Employee.findOne({ _id: req.params.employeeId }, (err, employee) => {
    if (err) {
      return next(err);
    }
    if (!employee) {
      res.render('employee/publications/update', {
        title: 'Employee Publication Details',
        user: req.user,
        employee: false
      });
    } else {
      Publication.findOne({ _id: req.params.publicationId }, (err, publication) => {
        if (err) { return next(err); }
        if (!publication) {
          res.render('employee/publications/update', {
            title: 'Employee Publication Details',
            user: req.user,
            employee,
            publication: false
          });
        } else {
          User.findOne({ _id: publication.gradedBy }, (err, graded) => {
            if (err) { return next(err); }
            const publicationJson = JSON.stringify(publication);
            res.render('employee/publications/update', {
              title: 'Employee Publication Update',
              user: req.user,
              employee,
              publication,
              graded,
              publicationJson
            });
          });
        }
      });
    }
  });
};


// Post new
/**
 *  POST /employee/review/new
 *  Create employee review
 */
exports.postEmployeeReviewNew = (req, res, next) => {
  if (!req.user && !req.body.employee) {
    return res.redirect('/employees');
  }
  req.assert('grade', 'Please enter a valid grade.').notEmpty();
  req.assert('consumed', 'Please enter consumed hours.').notEmpty();
  req.assert('estimate', 'Please enter please enter estimate points.').notEmpty();
  req.assert('description', 'Please enter a valid description.').notEmpty();
  req.assert('description', 'Description must have more then 3 characters.').isLength({ min: 4 });
  req.assert('employee', 'Please select a valid employee.').notEmpty();
  req.assert('title', 'Please enter a valid title of review.').notEmpty();
  req.assert('title', 'Review title must have at least characters.').isLength({ min: 3 });

  const errors = req.validationErrors();
  if (errors) {
    req.flash('errors', errors);
    return res.redirect('back');
  }

  Employee.findOne({ _id: req.body.employee }, (err, employee) => {
    if (err) {
      return next(err);
    }
    if (!employee) {
      req.flash('error', { msg: 'Employee has been deleted.' });
      return res.redirect('back');
    }
    // Create new review
    const review = new Review();
    review.data.estimate = req.body.estimate;
    review.data.consumed = req.body.consumed;
    review.data.description = req.body.description;
    review.data.title = req.body.title;
    review.employeeId = req.body.employee;
    review.gradedBy = req.user._id;

    // Attach the review to employee for easier lookup
    review.save((err) => {
      if (err) { return next(err); }
      employee.reviews.push(review._id);
      employee.save((err) => {
        if (err) { return next(err); }
        const activity = new Activity({
          name: req.user.email,
          slug: 'review-create',
          description: `User ${req.user.email} has reviewed an employee account ${employee.email} on ${Date()}`
        });
        activity.save((err) => {
          if (err) { return next(err); }
          req.flash('success', { msg: 'Employee has been reviewed.' });
          res.redirect(`/employee/${employee._id}/details`);
        });
      });
    });
  });
};

/**
 *  POST /employee/publication/new
 *  Create employee publication
 */
exports.postEmployeePublicationNew = (req, res, next) => {
  if (!req.body.user && !req.body.employee) {
    return res.redirect('/employees');
  }
  req.assert('grade', 'Please enter a valid grade.').notEmpty();
  req.assert('consumed', 'Please enter consumed hours.').notEmpty();
  req.assert('estimate', 'Please enter please enter estimate points.').notEmpty();
  req.assert('description', 'Please enter a valid description.').notEmpty();
  req.assert('description', 'Description must have more then 3 characters.').isLength({ min: 4 });
  req.assert('employee', 'Please select a valid employee.').notEmpty();
  req.assert('title', 'Please enter a valid title of publication.').notEmpty();
  req.assert('title', 'Publication title must have at least characters.').isLength({ min: 3 });

  const errors = req.validationErrors();
  if (errors) {
    req.flash('errors', errors);
    res.redirect('back');
  } else {
    Employee.findOne({ _id: req.body.employee }, (err, employee) => {
      if (err) {
        return next(err);
      }
      if (!employee) {
        req.flash('error', { msg: 'Employee has been deleted.' });
        return res.redirect('back');
      }
      // Attach the publication to employee for easier lookup
      // Create new review
      const publication = new Publication();
      publication.data.description = req.body.description;
      publication.data.title = req.body.title;
      publication.data.estimate = req.body.estimate;
      publication.data.consumed = req.body.consumed;
      publication.employeeId = req.body.employee;
      publication.gradedBy = req.body.gradedBy;
      // Attach the publication to employee for easier lookup
      publication.save((err) => {
        if (err) { return next(err); }
        employee.publications.push(publication._id);
        employee.save((err) => {
          if (err) { return next(err); }
          const activity = new Activity({
            name: req.body.gradedBy,
            slug: 'employee-add-publication',
            description: `User ${req.user.profile.email} has added publication an employee account ${employee.email} on ${Date()}`
          });
          activity.save((err) => {
            if (err) { return next(err); }
            req.flash('success', { msg: 'Added employee publication.' });
            res.redirect(`/employee/${employee._id}/details`);
          });
        });
      });
    });
  }
};

/**
 * POST /employee/role/new
 * Create new role.
 */
exports.postRole = (req, res, next) => {
  if (!req.user) {
    return res.redirect('/employee/roles');
  }
  req.assert('slug', 'Slug cannot be blank').notEmpty();
  req.assert('name', 'Name cannot be blank').notEmpty();
  req.assert('name', 'Name must be at least 3 characters long').isLength({ min: 3, max: 24 });
  req.assert('description', 'Description cannot be blank').notEmpty();
  req.assert('description', 'Description must have more then 2 characters.').isLength({ min: 3 });

  const errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('back');
  }

  const role = new Role({
    id: req.body.slug,
    slug: req.body.slug,
    name: req.body.name,
    description: req.body.description,
  });

  role.save((err) => {
    if (err) {
      return next(err);
    }
    const activity = new Activity({
      name: req.user.email,
      slug: 'role-create',
      description: `User ${req.user.email} has created a role ${role.name} on ${Date()}`
    });
    activity.save((err) => {
      if (err) {
        return next(err);
      }
      res.redirect('/employee/roles');
    });
  });
};

/**
 * POST /employee/new
 * Dashboard page.
 */
exports.postEmployee = (req, res, next) => {
  if (!req.user) {
    return res.redirect('/employees');
  }
  req.assert('email', 'Email can not be empty.').notEmpty();
  req.assert('email', 'Email is not valid. Please provide a valid email.').isEmail();
  req.assert('name', 'Name cannot be blank, nor less then 3 characters.').notEmpty();
  req.assert('name', 'Name must have at least 3 characters.').isLength({ min: 3 });
  req.assert('gender', 'Gender cannot be blank, must be male or female.').notEmpty();
  req.assert('phone', 'Phone number can not be empty.').notEmpty();
  req.assert('phone', 'Phone must be valid in numeric format.').isNumeric();
  req.assert('phone', 'Phone number must contain at least 6 digits').isLength({ min: 6 });

  const errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('back');
  }

  const employee = new Employee({
    email: req.body.email,
    profile: {
      name: req.body.name,
      gender: req.body.gender,
      phone: req.body.phone,
    }
  });
  if (req.body.address_line
      || req.body.address_city
      || req.body.address_state
      || req.body.address_postcode
      || req.body.address_country
  ) {
    const address = {
      line: req.body.address_line || '',
      city: req.body.address_city || '',
      state: req.body.address_state || '',
      postcode: req.body.address_postcode || '',
      country: req.body.address_country || '',
    };
    employee.setAddress(address);
  }
  if (req.body.role) {
    Role.findOne({ _id: req.body.role }, (err, roleObject) => {
      if (err) { return next(err); }
      if (roleObject) {
        employee.setRole(roleObject);
      }
    });
  } else {
    const role = { state: true };
    employee.setRole(role);
  }
  Employee.findOne({ email: req.body.email }, (err, existingEmployee) => {
    if (err) { return next(err); }
    if (existingEmployee) {
      req.flash('errors', { msg: 'Employee with that email address already exists.' });
      return res.redirect('back');
    }
    employee.save((err) => {
      if (err) {
        return next(err);
      }
      const activity = new Activity({
        name: req.user.email,
        slug: 'employee-create',
        description: `User ${req.user.email} has created a account for employee ${employee.email} on ${Date()}`
      });
      activity.save((err) => {
        if (err) {
          return next(err);
        }
        req.flash('success', { msg: 'Employee has been added.' });
        res.redirect('/employees');
      });
    });
  });
};


// Post update
/**
 * POST /employee/:_id/update
 * Update specific employee document
 */
exports.postEmployeeUpdate = (req, res, next) => {
  if (!req.user && !req.params.employeeId && !req.body) {
    return res.redirect('/employees');
  }
  req.assert('name', 'Name cannot be blank').notEmpty();
  req.assert('name', 'Name must contain at least 3 characters, name cannot be more then 30 characters').isLength({ min: 3, max: 30 });
  req.assert('gender', 'Gender cannot be blank').notEmpty();
  req.assert('phone', 'Phone number cannot be blank').notEmpty();
  req.assert('phone', 'Phone number cannot be less then 6 digits').isLength({ min: 6 });
  req.assert('phone', 'Phone number must be in numeric format').isNumeric();
  const errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('back');
  }

  Employee.findOne({ _id: req.params.employeeId }, (err, employee) => {
    if (err) {
      return next(err);
    }
    if (!employee) {
      req.flash('error', { msg: 'There was an issue saving the employee data.' });
      res.redirect('back');
    } else {
      employee.profile.name = req.body.name;
      employee.profile.gender = req.body.gender;
      employee.profile.phone = req.body.phone;
      Role.findOne({ _id: req.body.role }, (err, role) => {
        if (err) { return next(err); }
        if (!role) {
          req.flash('error', { msg: 'The role you selected, does not exist.' });
          return res.redirect('back');
        }
        employee.setRole(role);

        employee.save((err) => {
          if (err) { return next(err); }
          const activity = new Activity({
            name: req.user.email,
            slug: 'employee-update',
            description: `User ${req.user.email} has updated an employee account ${employee.email} on ${Date()}`
          });
          activity.save((err) => {
            if (err) { return next(err); }
            req.flash('success', { msg: 'Employee has been updated' });
            res.redirect(`/employee/${employee._id}/details`);
          });
        });
      });
    }
  });
};

/**
 * POST /employee/role/update
 * Update role.
 */
exports.updateRole = (req, res, next) => {
  if (!req.user && !req.body && !req.params.roleId) {
    return res.redirect('/employee/roles');
  }
  req.assert('name', 'Please enter a valid name.').notEmpty();
  req.assert('name', 'Name must contain at least 3 characters.').isLength({ min: 3 });
  req.assert('slug', 'Please enter a valid slug ( slug must not contain spaces, or end in special character ).').notEmpty();
  req.assert('description', 'Please enter a valid description.').notEmpty();
  const errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('back');
  }
  Role.findOne({ _id: req.params.roleId }, (err, role) => {
    if (err) {
      return next(err);
    }
    if (!role) {
      req.flash('error', { msg: 'There was an issue updating the role data.' });
      res.redirect('back');
    } else {
      role.name = req.body.name;
      role.slug = req.body.slug;
      role.description = req.body.description;
      role.save((err) => {
        if (err) { return next(err); }
        const activity = new Activity({
          name: req.body.email,
          slug: 'role-update',
          description: `User ${req.body.email} has updated the role ${role.name} on ${Date()}`
        });
        activity.save((err) => {
          if (err) { return next(err); }
          req.flash('success', { msg: 'Role has been successfully updated.' });
          res.redirect('/employee/roles');
        });
      });
    }
  });
};

/**
 *  POST /employee/review/update
 *  Update employee review
 */
exports.postEmployeeReviewUpdate = (req, res, next) => {
  if (!req.user && req.params.reviewId) {
    return res.redirect('/employees');
  }
  req.assert('grade', 'Please enter a grade number.').notEmpty();
  req.assert('consumed', 'Please enter consumed hours.').notEmpty();
  req.assert('estimate', 'Please enter please enter estimate points.').notEmpty();
  req.assert('title', 'Please enter a valid title.').notEmpty();
  req.assert('title', 'Title must have at least 3 characters, and can not be longer then 30 characters.').isLength({ min: 3, max: 32 });
  req.assert('description', 'Please enter a valid description.').notEmpty();
  req.assert('description', 'Description must have at least 4 characters.').isLength({ min: 4 });
  const errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('back');
  }
  // Create new review
  Review.findOne({ _id: req.params.reviewId }, (err, review) => {
    if (err) {
      return next(err);
    }
    review.data.description = req.body.description;
    review.data.title = req.body.title;
    review.data.consumed = req.body.consumed;
    review.data.estimate = req.body.estimate;
    review.save((err) => {
      if (err) {
        return next(err);
      }
      const activity = new Activity({
        name: req.user.email,
        slug: 'review-update',
        description: `User ${req.user.email} has updated a reviewed of employee on ${Date()}`
      });
      activity.save((err) => {
        if (err) {
          return next(err);
        }
        req.flash('info', { msg: 'Review has been updated.' });
        res.redirect(`/employee/${review.employeeId}/details`);
      });
    });
  });
};

/**
 *  POST /employee/publication/update
 *  Update employee publication
 */
exports.postEmployeePublicationUpdate = (req, res, next) => {
  if (!req.user && req.params.publicationId) {
    return res.redirect('/employees');
  }
  req.assert('grade', 'Please enter a grade number.').notEmpty();
  req.assert('consumed', 'Please enter consumed hours.').notEmpty();
  req.assert('estimate', 'Please enter please enter estimate points.').notEmpty();
  req.assert('title', 'Please enter a valid title.').notEmpty();
  req.assert('title', 'Title must have at least 3 characters, and can not be more then 30.').isLength({ min: 3, max: 32 });
  req.assert('description', 'Please enter a valid description.').notEmpty();
  req.assert('description', 'Description must have at least 4 characters.').isLength({ min: 4 });
  const errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('back');
  }
  Publication.findOne({ _id: req.params.publicationId }, (err, publication) => {
    if (err) {
      return next(err);
    }
    publication.data.description = req.body.description;
    publication.data.title = req.body.title;
    publication.data.estimate = req.body.estimate;
    publication.data.consumed = req.body.consumed;
    // publication.preUpdate(publication);
    publication.save((err) => {
      if (err) {
        return next(err);
      }
      const activity = new Activity({
        name: req.user.email,
        slug: 'publication-update',
        description: `User ${req.user.email} has updated a publication of employee on ${Date()}`
      });
      activity.save((err) => {
        if (err) {
          return next(err);
        }
        req.flash('info', { msg: 'Publication has been updated.' });
        res.redirect(`/employee/${publication.employeeId}/details`);
      });
    });
  });
};


// Post Get delete
/**
 * GET /employee/:_id/delete
 * Delete employee document.
 */
exports.EmployeeDelete = (req, res, next) => {
  if (!req.user && !req.params.employeeId) {
    return res.redirect('/employees');
  }
  // Find and delete employee account
  Employee.findOneAndDelete({ _id: req.params.employeeId }, (err, employee) => {
    if (err) { return next(err); }
    const activity = new Activity({
      name: req.user.email,
      slug: 'employee-delete',
      description: `User ${req.user.email} has deleted an employee account ${employee.email} on ${Date()}`
    });
    activity.save((err) => {
      if (err) { return next(err); }
      req.flash('info', { msg: 'Employee has been deleted.' });
      res.redirect('back');
    });
  });
};

/**
 * GET employee/role/:role_id/delete
 * Delete role document.
 */
exports.RoleDelete = (req, res, next) => {
  if (!req.user && !req.params.roleId) {
    return res.redirect('/employee/roles');
  }
  // Find and delete employee account
  Role.findOneAndDelete({ _id: req.params.roleId }, (err, role) => {
    if (err) { return next(err); }
    const activity = new Activity({
      name: req.user.email,
      slug: 'role-delete',
      description: `User ${req.user.email} has deleted an role: ${role.name} on ${Date()}`
    });
    activity.save((err) => {
      if (err) { return next(err); }
      req.flash('info', { msg: 'Role has been deleted.' });
      res.redirect('back');
    });
  });
};

/**
 * GET /employee/review/:_reviewId/delete
 * Delete employee review.
 */
exports.deleteReviewById = (req, res, next) => {
  if (!req.user && !req.params.reviewId) {
    return res.redirect('/employees');
  }
  // Find and delete employee account
  Review.findOneAndDelete({ _id: req.params.reviewId }, (err) => {
    if (err) { return next(err); }
    const activity = new Activity({
      name: req.user.email,
      slug: 'review-delete',
      description: `User ${req.user.email} has deleted a review on ${Date()}`
    });
    activity.save((err) => {
      if (err) { return next(err); }
      req.flash('info', { msg: 'Review has been deleted.' });
      res.redirect('back');
    });
  });
};

/**
 * GET /employee/publication/:_publicationId/delete
 * Delete employee publication.
 */
exports.deletePublicationById = (req, res, next) => {
  if (!req.user && !req.params.publicationId) {
    return res.redirect('/employees');
  }
  // Find and delete employee account
  Publication.findOneAndDelete({ _id: req.params.publicationId }, (err) => {
    if (err) { return next(err); }
    const activity = new Activity({
      name: req.user.email,
      slug: 'publication-delete',
      description: `User ${req.user.email} has deleted a publication on ${Date()}`
    });
    activity.save((err) => {
      if (err) { return next(err); }
      req.flash('info', { msg: 'Publication has been deleted.' });
      res.redirect('back');
    });
  });
};
