/**
 * Module dependencies.
 */
const express = require('express');
const compression = require('compression');
const session = require('express-session');
const bodyParser = require('body-parser');
const logger = require('morgan');
const chalk = require('chalk');
const errorHandler = require('errorhandler');
const lusca = require('lusca');
const dotenv = require('dotenv');
const MongoStore = require('connect-mongo')(session);
const flash = require('express-flash');
const path = require('path');
const mongoose = require('mongoose');
const passport = require('passport');
const expressValidator = require('express-validator');
const expressStatusMonitor = require('express-status-monitor');
const sass = require('node-sass-middleware');
const multer = require('multer');

const upload = multer({ dest: path.join(__dirname, 'uploads') });

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */
dotenv.load({ path: '.env' });

/**
 * Controllers (route handlers).
 */
const userController = require('./controllers/user');
const adminController = require('./controllers/admin');
const employeeController = require('./controllers/employee');
const activityController = require('./controllers/activity');
const dashboardController = require('./controllers/dashboard');
const comparisonController = require('./controllers/comparison');

/**
 * API keys and Passport configuration.
 */
const passportConfig = require('./config/passport');

/**
 * Create Express server.
 */
const app = express();

/**
 * Connect to MongoDB.
 */
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useNewUrlParser', true);
mongoose.connect(process.env.MONGODB_URI);
mongoose.connection.on('error', (err) => {
  console.error(err);
  console.log('%s MongoDB connection error. Please make sure MongoDB is running.', chalk.red('✗'));
  process.exit();
});

/**
 * Express configuration.
 */
app.set('host', process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0');
app.set('port', process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(expressStatusMonitor());
app.use(compression());
app.use(sass({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public')
}));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET,
  cookie: { maxAge: 1209600000 }, // two weeks in milliseconds
  store: new MongoStore({
    url: process.env.MONGODB_URI,
    autoReconnect: true,
  })
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use((req, res, next) => {
  if (req.path === '/api/upload') {
    next();
  } else {
    lusca.csrf()(req, res, next);
  }
});
app.use(lusca.xframe('SAMEORIGIN'));
app.use(lusca.xssProtection(true));
app.disable('x-powered-by');
app.use((req, res, next) => {
  res.locals.user = req.user;
  next();
});
app.use((req, res, next) => {
  // After successful login, redirect back to the account page
  if (!req.user
    && req.path !== '/login'
    && req.path !== '/signup'
    && !req.path.match(/^\/auth/)
    && !req.path.match(/\./)) {
    req.session.returnTo = '/account';
  } else if (req.user
    && (req.path === '/account' || req.path.match(/^\/api/))) {
    req.session.returnTo = '/account';
  }
  next();
});
app.use('/', express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 }));
app.use('/js/lib', express.static(path.join(__dirname, 'node_modules/popper.js/dist/umd'), { maxAge: 31557600000 }));
app.use('/js/lib', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/js'), { maxAge: 31557600000 }));
app.use('/js/lib', express.static(path.join(__dirname, 'node_modules/jquery/dist'), { maxAge: 31557600000 }));
app.use('/webfonts', express.static(path.join(__dirname, 'node_modules/@fortawesome/fontawesome-free/webfonts'), { maxAge: 31557600000 }));

/**
 * Primary app routes.
 */
app.get('/login', userController.getLogin);
app.post('/login', userController.postLogin);
app.get('/logout', userController.logout);
app.get('/reset', userController.reset);
app.get('/signup', userController.getSignup);
app.post('/signup', userController.postSignup);

app.get('/', userController.getHomepage);

app.get('/account', passportConfig.isAuthenticated, userController.getAccount);
app.post('/account/profile', passportConfig.isAuthenticated, userController.postUpdateProfile);
app.post('/admin/new', passportConfig.isAuthenticated, adminController.postAdmin);
app.post('/account/password', passportConfig.isAuthenticated, userController.postUpdatePassword);
app.post('/account/delete', passportConfig.isAuthenticated, userController.postDeleteAccount);

// Dashboard
app.get('/dashboard', passportConfig.isAuthenticated, dashboardController.getDashboard);
app.get('/comparison', passportConfig.isAuthenticated, comparisonController.getComparison);

app.get('/activity', passportConfig.isAuthenticated, activityController.getActivityLog);
app.get('/administration', passportConfig.isAuthenticated, adminController.getAdmin);
app.get('/admin/new', passportConfig.isAuthenticated, adminController.getAdminNew);
app.post('/admin/delete', passportConfig.isAuthenticated, userController.postDeleteAdmin);
app.get('/admin/:adminId/delete', passportConfig.isAuthenticated, adminController.AdminDelete);


/**
 * Employees
 */
app.get('/employees', passportConfig.isAuthenticated, employeeController.getEmployeeList);
app.get('/employee/:employeeId/details', passportConfig.isAuthenticated, employeeController.getEmployeeById);
app.get('/employee/:employeeId/delete', passportConfig.isAuthenticated, employeeController.EmployeeDelete);
app.post('/employee/:employeeId/update', passportConfig.isAuthenticated, employeeController.postEmployeeUpdate);

/*
* Reviews
*/
app.get('/employee/review/new', passportConfig.isAuthenticated, employeeController.getEmployeeReviewNew);
app.post('/employee/review/new', passportConfig.isAuthenticated, employeeController.postEmployeeReviewNew);
app.get('/employee/:employeeId/review/:reviewId/details', passportConfig.isAuthenticated, employeeController.getEmployeeReview);
app.get('/employee/:employeeId/reviews', passportConfig.isAuthenticated, employeeController.getEmployeeReviewList);
app.get('/employee/:employeeId/review/:reviewId/update', passportConfig.isAuthenticated, employeeController.getEmployeeReviewUpdate);
app.post('/employee/review/:reviewId/update', passportConfig.isAuthenticated, employeeController.postEmployeeReviewUpdate);
app.get('/employee/review/:reviewId/delete', passportConfig.isAuthenticated, employeeController.deleteReviewById);


app.get('/employee/publication/new', passportConfig.isAuthenticated, employeeController.getEmployeePublicationNew);
app.post('/employee/publication/new', passportConfig.isAuthenticated, employeeController.postEmployeePublicationNew);
app.get('/employee/:employeeId/publication/:publicationId/details', passportConfig.isAuthenticated, employeeController.getEmployeePublication);
app.get('/employee/:employeeId/publications', passportConfig.isAuthenticated, employeeController.getEmployeePublicationList);
app.get('/employee/:employeeId/publication/:publicationId/update', passportConfig.isAuthenticated, employeeController.getEmployeePublicationUpdate);
app.post('/employee/publication/:publicationId/update', passportConfig.isAuthenticated, employeeController.postEmployeePublicationUpdate);
app.get('/employee/publication/:publicationId/delete', passportConfig.isAuthenticated, employeeController.deletePublicationById);


app.post('/employee/new', passportConfig.isAuthenticated, employeeController.postEmployee);
app.post('/employee/role/new', passportConfig.isAuthenticated, employeeController.postRole);
app.post('/employee/role/:roleId/update', passportConfig.isAuthenticated, employeeController.updateRole);

app.get('/employee/role/:roleId/details', passportConfig.isAuthenticated, employeeController.getRoleById);
app.get('/employee/role/:roleId/delete', passportConfig.isAuthenticated, employeeController.RoleDelete);

app.get('/employee/new', passportConfig.isAuthenticated, employeeController.getEmployeeNew);
app.get('/employee/roles', passportConfig.isAuthenticated, employeeController.getEmployeeRoles);
app.get('/employee/role/new', passportConfig.isAuthenticated, employeeController.getRoleNew);

/**
 * Error Handler.
 */
if (process.env.NODE_ENV === 'development') {
  // only use in development
  app.use(errorHandler());
} else {
  app.use((err, req, res, next) => {
    console.error(err);
    res.status(500).send('Server Error');
  });
}

/**
 * Start Express server.
 */
app.listen(app.get('port'), () => {
  console.log('%s App is running at http://localhost:%d in %s mode', chalk.green('✓'), app.get('port'), app.get('env'));
  console.log('  Press CTRL-C to stop\n');
});

module.exports = app;
