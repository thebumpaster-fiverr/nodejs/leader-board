//= = Class definition
const LayoutBuilder = (function () {
  var exporter = {
    init() {
      $('#m-btn-howto').click((e) => {
        e.preventDefault();
        $('#m-howto').slideToggle();
      });
      this.exportHtml();
      this.exportHtmlStatic();
    },
    startLoad(options) {
      $('#builder_export')
        .addClass('m-loader m-loader--light m-loader--right')
        .find('> span > span')
        .text('Exporting...')
        .closest('.m-form__actions')
        .find('.btn')
        .prop('disabled', true);
      $.notify(options);
    },
    doneLoad() {
      $('#builder_export')
        .removeClass('m-loader m-loader--light m-loader--right')
        .find('> span > span')
        .text('Export')
        .closest('.m-form__actions')
        .find('.btn')
        .prop('disabled', false);
    },
    exportHtml() {
      $('#builder_export_html').click(function (e) {
        e.preventDefault();
        const purchaseCode = $('#purchase-code').val();
        if (!purchaseCode) return;

        const _self = $(this);

        exporter.startLoad({
          title: 'Generate HTML Partials',
          message: 'Process started and it may take about 1 to 10 minutes.',
        });

        $.ajax('index.php', {
          method: 'POST',
          data: {
            builder_export: 1,
            export_type: 'partial',
            demo: $(_self).data('demo'),
            purchase_code: purchaseCode,
          },
        }).done((r) => {
          const result = JSON.parse(r);
          if (result.message) {
            exporter.stopWithNotify(result.message);
            return;
          }

          verify.setItem('purchase_code', purchaseCode);
          var timer = setInterval(() => {
            $.ajax('index.php', {
              method: 'POST',
              data: {
                builder_export: 1,
                builder_check: result.id,
              },
            }).done((r) => {
              const result = JSON.parse(r);
              if (typeof result === 'undefined') return;
              // export status 1 is completed
              if (result.export_status !== 1) return;

              $('<iframe/>').attr({
                src: `index.php?builder_export&builder_download&id=${
                  result.id}`,
                style: 'visibility:hidden;display:none',
              }).ready(() => {
                $.notify({
                  title: 'Export HTML Version Layout',
                  message: 'HTML version exported.',
                }, { type: 'success' });
                exporter.doneLoad();
                // stop the timer
                clearInterval(timer);
              }).appendTo(_self);
            });
          }, 15000);
        });
      });
    },
    exportHtmlStatic() {
      $('#builder_export_html_static').click(function (e) {
        e.preventDefault();
        const purchaseCode = $('#purchase-code').val();
        if (!purchaseCode) return;

        const _self = $(this);

        exporter.startLoad({
          title: 'Generate HTML Static Version',
          message: 'Process started and it may take about 1 to 10 minutes.',
        });

        $.ajax('index.php', {
          method: 'POST',
          data: {
            builder_export: 1,
            export_type: 'html',
            demo: $(_self).data('demo'),
            purchase_code: purchaseCode,
          },
        }).done((r) => {
          const result = JSON.parse(r);
          if (result.message) {
            exporter.stopWithNotify(result.message);
            return;
          }

          verify.setItem('purchase_code', purchaseCode);
          var timer = setInterval(() => {
            $.ajax('index.php', {
              method: 'POST',
              data: {
                builder_export: 1,
                builder_check: result.id,
              },
            }).done((r) => {
              const result = JSON.parse(r);
              if (typeof result === 'undefined') return;
              // export status 1 is completed
              if (result.export_status !== 1) return;

              $('<iframe/>').attr({
                src: `index.php?builder_export&builder_download&id=${
                  result.id}`,
                style: 'visibility:hidden;display:none',
              }).ready(() => {
                $.notify({
                  title: 'Export Default Version',
                  message: 'Default HTML version exported with current configured layout.',
                }, { type: 'success' });
                exporter.doneLoad();
                // stop the timer
                clearInterval(timer);
              }).appendTo(_self);
            });
          }, 15000);
        });
      });
    },
    stopWithNotify(message, type) {
      type = type || 'danger';
      $.notify({
        title: 'Verification failed',
        message,
      }, { type });
      exporter.doneLoad();
    },
  };

  //= = Private functions
  const preview = function () {
    $('[name="builder_submit"]').click(function (e) {
      e.preventDefault();
      const _self = $(this);
      $(_self)
        .addClass('m-loader m-loader--light m-loader--right')
        .closest('.m-form__actions')
        .find('.btn')
        .prop('disabled', true);

      $.ajax(`index.php?demo=${$(_self).data('demo')}`, {
        method: 'POST',
        data: $('[name]').serialize(),
      }).done((r) => {
        $.notify({
          title: 'Preview updated',
          message: 'Preview has been updated with current configured layout.',
        }, { type: 'success' });
      }).always(() => {
        setTimeout(() => {
          location.reload();
        }, 600);
      });
    });
  };

  const reset = function () {
    $('[name="builder_reset"]').click(function (e) {
      e.preventDefault();
      const _self = $(this);
      $(_self)
        .addClass('m-loader m-loader--primary m-loader--right')
        .closest('.m-form__actions')
        .find('.btn')
        .prop('disabled', true);

      $.ajax(`index.php?demo=${$(_self).data('demo')}`, {
        method: 'POST',
        data: {
          builder_reset: 1,
          demo: $(_self).data('demo'),
        },
      }).done((r) => {
      }).always(() => {
        location.reload();
      });
    });
  };

  const keepActiveTab = function () {
    $('[href^="#m_builder_"]').click(function (e) {
      const which = $(this).attr('href');
      const btn = $('[name="builder_submit"]');
      const tab = $('[name="builder[tab]"]');
      if ($(tab).length === 0) {
        $('<input/>')
          .attr('type', 'hidden')
          .attr('name', 'builder[tab]')
          .val(which)
          .insertBefore(btn);
      } else {
        $(tab).val(which);
      }
    }).each(function () {
      if ($(this).hasClass('active')) {
        const which = $(this).attr('href');
        const btn = $('[name="builder_submit"]');
        const tab = $('[name="builder[tab]"]');
        if ($(tab).length === 0) {
          $('<input/>')
            .attr('type', 'hidden')
            .attr('name', 'builder[tab]')
            .val(which)
            .insertBefore(btn);
        } else {
          $(tab).val(which);
        }
      }
    });
  };

  // localStorage.removeItem('envato');
  var verify = {
    expires_in: 3600,
    isVerified() {
      // check token is not expired and verified
      return localStorage.getItem('envato');
    },
    reCaptchaVerified() {
      return $.ajax('../tools/builder/recaptcha.php?recaptcha', {
        method: 'POST',
        data: {
          response: $('#g-recaptcha-response').val(),
        },
      }).fail(() => {
        grecaptcha.reset();
        $('#alert-message')
          .removeClass('alert-success m--hide')
          .addClass('alert-danger')
          .html('Invalid reCaptcha validation');
      });
    },
    init() {
      $('#purchase-code').keyup(() => {
        $('#alert-message').addClass('m--hide');
      }).val(verify.getItem('purchase_code'));

      let exportReadyTrigger;
      // click event
      $('#builder_export').closest('.dropdown').find('.dropdown-item').click(function (e) {
        e.preventDefault();
        exportReadyTrigger = $(this);

        if (verify.isVerified()) return;

        $('#m-modal-purchase').modal('show');
        $('#alert-message').addClass('m--hide');
        grecaptcha.reset();
      });

      $('#submit-verify').click((e) => {
        e.preventDefault();
        verify.reCaptchaVerified().done((response) => {
          if (response.success) {
            $('[data-dismiss="modal"]').trigger('click');
            $(exportReadyTrigger).trigger('click');
          } else {
            grecaptcha.reset();
            $('#alert-message')
              .removeClass('alert-success m--hide')
              .addClass('alert-danger')
              .html('Invalid reCaptcha validation');
          }
        });
      });
    },
    setItems(object) {
      const params = $.extend({}, verify.getItem(), object);
      localStorage.setItem('envato', JSON.stringify(params));
    },
    setItem(key, val) {
      const assign = {};
      assign[key] = val;
      const params = $.extend({}, verify.getItem(), assign);
      localStorage.setItem('envato', JSON.stringify(params));
    },
    getItem(key) {
      const params = JSON.parse(localStorage.getItem('envato'));
      if (typeof key !== 'undefined') {
        return params !== null ? params[key] : null;
      }
      return params;
    },
    startTimer(now) {
      verify.setItem('created_on', now);
      setTimeout(() => {
        localStorage.removeItem('envato');
      }, verify.expires_in * 1000);
    },
    tokenIsExpired() {
      const diff = new Date().getTime() - verify.getItem('created_on');
      if (typeof verify.expires_in === 'undefined'
				|| diff >= (verify.expires_in * 1000)) {
        localStorage.removeItem('envato');
        return true;
      }
      return false;
    },
  };

  // basic demo
  const init = function () {
    exporter.init();
    keepActiveTab();
    preview();
    reset();
  };

  return {
    // public functions
    init() {
      verify.init();
      init();
    },
  };
}());

jQuery(document).ready(() => {
  LayoutBuilder.init();
});
