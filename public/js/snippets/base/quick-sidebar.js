const mQuickSidebar = (function () {
  const topbarAside = $('#m_quick_sidebar');
  const topbarAsideTabs = $('#m_quick_sidebar_tabs');
  const topbarAsideContent = topbarAside.find('.m-quick-sidebar__content');

  const initMessages = function () {
    const messages = mUtil.find(mUtil.get('m_quick_sidebar_tabs_messenger'), '.m-messenger__messages');
    const form = $('#m_quick_sidebar_tabs_messenger .m-messenger__form');

    mUtil.scrollerInit(messages, {
      disableForMobile: true,
      resetHeightOnDestroy: false,
      handleWindowResize: true,
      height() {
        const height = topbarAside.outerHeight(true)
                    - topbarAsideTabs.outerHeight(true)
                    - form.outerHeight(true) - 120;

        return height;
      }
    });
  };

  const initSettings = function () {
    const settings = mUtil.find(mUtil.get('m_quick_sidebar_tabs_settings'), '.m-list-settings');

    if (!settings) {
      return;
    }

    mUtil.scrollerInit(settings, {
      disableForMobile: true,
      resetHeightOnDestroy: false,
      handleWindowResize: true,
      height() {
        return mUtil.getViewPort().height - topbarAsideTabs.outerHeight(true) - 60;
      }
    });
  };

  const initLogs = function () {
    const logs = mUtil.find(mUtil.get('m_quick_sidebar_tabs_logs'), '.m-list-timeline');

    if (!logs) {
      return;
    }

    mUtil.scrollerInit(logs, {
      disableForMobile: true,
      resetHeightOnDestroy: false,
      handleWindowResize: true,
      height() {
        return mUtil.getViewPort().height - topbarAsideTabs.outerHeight(true) - 60;
      }
    });
  };

  const initOffcanvasTabs = function () {
    initMessages();
    initSettings();
    initLogs();
  };

  const initOffcanvas = function () {
    const topbarAsideObj = new mOffcanvas('m_quick_sidebar', {
      overlay: true,
      baseClass: 'm-quick-sidebar',
      closeBy: 'm_quick_sidebar_close',
      toggleBy: 'm_quick_sidebar_toggle'
    });

    // run once on first time dropdown shown
    topbarAsideObj.one('afterShow', () => {
      mApp.block(topbarAside);

      setTimeout(() => {
        mApp.unblock(topbarAside);

        topbarAsideContent.removeClass('m--hide');

        initOffcanvasTabs();
      }, 1000);
    });
  };

  return {
    init() {
      if (topbarAside.length === 0) {
        return;
      }

      initOffcanvas();
    }
  };
}());

$(document).ready(() => {
  mQuickSidebar.init();
});
