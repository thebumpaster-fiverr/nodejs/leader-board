const mMail = (function () {
  let asideOffcanvas;
  let asideMenu;

  //= = Aside
  const initAside = function () {
    //= = Init offcanvas aside for mobile mode
    asideOffcanvas = new mOffcanvas('m_mail_aside', {
      overlay: true,
      baseClass: 'm-mail__aside',
      closeBy: 'm_mail_aside_close_btn',
      toggleBy: {
        target: 'm_mail_aside_toggle_btn',
        state: 'm-mail-aside-toggle--active'
      }
    });
  };

  //= = Aside Menu
  const initAsideMenu = function () {
    asideMenu = new mMenu('m_mail_aside_menu', {
      submenu: {
        desktop: 'dropdown',
        tablet: 'accordion',
        mobile: 'accordion'
      },
      accordion: {
        slideSpeed: 200, // accordion toggle slide speed in milliseconds
        autoScroll: true, // enable auto scrolling(focus) to the clicked menu item
        expandAll: false // allow having multiple expanded accordions in the menu
      }
    });
  };

  return {
    init() {
      //= = Init components
      initAside();
      initAsideMenu();
    }
  };
}());

$(document).ready(() => {
  if (mUtil.isAngularVersion() === false) {
    mMail.init();
  }
});
