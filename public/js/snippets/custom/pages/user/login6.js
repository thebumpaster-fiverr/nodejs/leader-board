//= = Class Definition
const SnippetLogin = (function () {
  const login = $('#m_login');

  const showErrorMsg = function (form, type, msg) {
    const alert = $(`<div class="m-alert m-alert--outline alert alert-${type} alert-dismissible" role="alert">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
			<span></span>\
		</div>`);

    form.find('.alert').remove();
    alert.prependTo(form);
    // alert.animateClass('fadeIn animated');
    mUtil.animateClass(alert[0], 'fadeIn animated');
    alert.find('span').html(msg);
  };

  //= = Private Functions

  const handleSignInFormSubmit = function () {
    $('#m_login_signin_submit').click(function (e) {
      e.preventDefault();
      const btn = $(this);
      const form = $('.m-login__form');

      form.validate({
        rules: {
          username: {
            required: true
          },
          password: {
            required: true
          }
        }
      });

      if (!form.valid()) {
        return;
      }

      btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

      form.ajaxSubmit({
        url: '',
        success(response, status, xhr, $form) {
          // similate 2s delay
          setTimeout(() => {
            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
            showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');
          }, 2000);
        }
      });
    });
  };

  //= = Public Functions
  return {
    // public functions
    init() {
      handleSignInFormSubmit();
    }
  };
}());

//= = Class Initialization
jQuery(document).ready(() => {
  SnippetLogin.init();
});
