/**
 * @class mApp  Metronic App class
 */

var mApp = (function () {
  /** @type {object} colors State colors * */
  let colors = {
    brand: '#716aca',
    metal: '#c4c5d6',
    light: '#ffffff',
    accent: '#00c5dc',
    primary: '#5867dd',
    success: '#34bfa3',
    info: '#36a3f7',
    warning: '#ffb822',
    danger: '#f4516c',
    focus: '#9816f4'
  };

  /**
    * Initializes bootstrap tooltip
    */
  const initTooltip = function (el) {
    const skin = el.data('skin') ? `m-tooltip--skin-${el.data('skin')}` : '';
    const width = el.data('width') == 'auto' ? 'm-tooltop--auto-width' : '';
    const triggerValue = el.data('trigger') ? el.data('trigger') : 'hover';
    const placement = el.data('placement') ? el.data('placement') : 'left';

    el.tooltip({
      trigger: triggerValue,
      template: `<div class="m-tooltip ${skin} ${width} tooltip" role="tooltip">\
                <div class="arrow"></div>\
                <div class="tooltip-inner"></div>\
            </div>`
    });
  };

  /**
    * Initializes bootstrap tooltips
    */
  const initTooltips = function () {
    // init bootstrap tooltips
    $('[data-toggle="m-tooltip"]').each(function () {
      initTooltip($(this));
    });
  };

  /**
    * Initializes bootstrap popover
    */
  const initPopover = function (el) {
    const skin = el.data('skin') ? `m-popover--skin-${el.data('skin')}` : '';
    const triggerValue = el.data('trigger') ? el.data('trigger') : 'hover';

    el.popover({
      trigger: triggerValue,
      template: `\
            <div class="m-popover ${skin} popover" role="tooltip">\
                <div class="arrow"></div>\
                <h3 class="popover-header"></h3>\
                <div class="popover-body"></div>\
            </div>`
    });
  };

  /**
    * Initializes bootstrap popovers
    */
  const initPopovers = function () {
    // init bootstrap popover
    $('[data-toggle="m-popover"]').each(function () {
      initPopover($(this));
    });
  };

  /**
    * Initializes bootstrap file input
    */
  const initFileInput = function () {
    // init bootstrap popover
    $('.custom-file-input').on('change', function () {
      const fileName = $(this).val();
      $(this).next('.custom-file-label').addClass('selected').html(fileName);
    });
  };

  /**
    * Initializes metronic portlet
    */
  const initPortlet = function (el, options) {
    // init portlet tools
    var el = $(el);
    const portlet = new mPortlet(el[0], options);
  };

  /**
    * Initializes metronic portlets
    */
  const initPortlets = function () {
    // init portlet tools
    $('[m-portlet="true"]').each(function () {
      const el = $(this);

      if (el.data('portlet-initialized') !== true) {
        initPortlet(el, {});
        el.data('portlet-initialized', true);
      }
    });
  };

  /**
    * Initializes scrollable contents
    */
  const initScrollers = function () {
    $('[data-scrollable="true"]').each(function () {
      const el = $(this);
      mUtil.scrollerInit(this, {
        disableForMobile: true,
        handleWindowResize: true,
        height() {
          if (mUtil.isInResponsiveRange('tablet-and-mobile') && el.data('mobile-height')) {
            return el.data('mobile-height');
          }
          return el.data('height');
        }
      });
    });
  };

  /**
    * Initializes bootstrap alerts
    */
  const initAlerts = function () {
    // init bootstrap popover
    $('body').on('click', '[data-close=alert]', function () {
      $(this).closest('.alert').hide();
    });
  };

  /**
    * Initializes Metronic custom tabs
    */
  const initCustomTabs = function () {
    // init bootstrap popover
    $('[data-tab-target]').each(function () {
      if ($(this).data('tabs-initialized') == true) {
        return;
      }

      $(this).click(function (e) {
        e.preventDefault();

        const tab = $(this);
        const tabs = tab.closest('[data-tabs="true"]');
        const contents = $(tabs.data('tabs-contents'));
        const content = $(tab.data('tab-target'));

        tabs.find('.m-tabs__item.m-tabs__item--active').removeClass('m-tabs__item--active');
        tab.addClass('m-tabs__item--active');

        contents.find('.m-tabs-content__item.m-tabs-content__item--active').removeClass('m-tabs-content__item--active');
        content.addClass('m-tabs-content__item--active');
      });

      $(this).data('tabs-initialized', true);
    });
  };

  const hideTouchWarning = function () {
    jQuery.event.special.touchstart = {
      setup(_, ns, handle) {
        if (typeof this === 'function') {
          if (ns.includes('noPreventDefault')) {
            this.addEventListener('touchstart', handle, { passive: false });
          } else {
            this.addEventListener('touchstart', handle, { passive: true });
          }
        }
      },
    };
    jQuery.event.special.touchmove = {
      setup(_, ns, handle) {
        if (typeof this === 'function') {
          if (ns.includes('noPreventDefault')) {
            this.addEventListener('touchmove', handle, { passive: false });
          } else {
            this.addEventListener('touchmove', handle, { passive: true });
          }
        }
      },
    };
    jQuery.event.special.wheel = {
      setup(_, ns, handle) {
        if (typeof this === 'function') {
          if (ns.includes('noPreventDefault')) {
            this.addEventListener('wheel', handle, { passive: false });
          } else {
            this.addEventListener('wheel', handle, { passive: true });
          }
        }
      },
    };
  };

  return {
    /**
        * Main class initializer
        */
    init(options) {
      if (options && options.colors) {
        colors = options.colors;
      }
      mApp.initComponents();
    },

    /**
        * Initializes components
        */
    initComponents() {
      hideTouchWarning();
      initScrollers();
      initTooltips();
      initPopovers();
      initAlerts();
      initPortlets();
      initFileInput();
      initCustomTabs();
    },


    /**
        * Init custom tabs
        */
    initCustomTabs() {
      initCustomTabs();
    },

    /**
        *
        * @param {object} el jQuery element object
        */
    // wrJangoer function to scroll(focus) to an element
    initTooltips() {
      initTooltips();
    },

    /**
        *
        * @param {object} el jQuery element object
        */
    // wrJangoer function to scroll(focus) to an element
    initTooltip(el) {
      initTooltip(el);
    },

    /**
        *
        * @param {object} el jQuery element object
        */
    // wrJangoer function to scroll(focus) to an element
    initPopovers() {
      initPopovers();
    },

    /**
        *
        * @param {object} el jQuery element object
        */
    // wrJangoer function to scroll(focus) to an element
    initPopover(el) {
      initPopover(el);
    },

    /**
        *
        * @param {object} el jQuery element object
        */
    // function to init portlet
    initPortlet(el, options) {
      initPortlet(el, options);
    },

    /**
        *
        * @param {object} el jQuery element object
        */
    // function to init portlets
    initPortlets() {
      initPortlets();
    },

    /**
        * Blocks element with loading indiciator using http://malsup.com/jquery/block/
        * @param {object} target jQuery element object
        * @param {object} options
        */
    block(target, options) {
      var el = $(target);

      options = $.extend(true, {
        opacity: 0.03,
        overlayColor: '#000000',
        state: 'brand',
        type: 'loader',
        size: 'lg',
        centerX: true,
        centerY: true,
        message: '',
        shadow: true,
        width: 'auto'
      }, options);

      let skin;
      let state;
      let loading;

      if (options.type == 'spinner') {
        skin = options.skin ? `m-spinner--skin-${options.skin}` : '';
        state = options.state ? `m-spinner--${options.state}` : '';
        loading = `<div class="m-spinner ${skin} ${state}"></div`;
      } else {
        skin = options.skin ? `m-loader--skin-${options.skin}` : '';
        state = options.state ? `m-loader--${options.state}` : '';
        size = options.size ? `m-loader--${options.size}` : '';
        loading = `<div class="m-loader ${skin} ${state} ${size}"></div`;
      }

      if (options.message && options.message.length > 0) {
        const classes = `m-blockui ${options.shadow === false ? 'm-blockui-no-shadow' : ''}`;

        html = `<div class="${classes}"><span>${options.message}</span><span>${loading}</span></div>`;

        var el = document.createElement('div');
        mUtil.get('body').prepend(el);
        mUtil.addClass(el, classes);
        el.innerHTML = `<span>${options.message}</span><span>${loading}</span>`;
        options.width = mUtil.actualWidth(el) + 10;
        mUtil.remove(el);

        if (target == 'body') {
          html = `<div class="${classes}" style="margin-left:-${options.width / 2}px;"><span>${options.message}</span><span>${loading}</span></div>`;
        }
      } else {
        html = loading;
      }

      const params = {
        message: html,
        centerY: options.centerY,
        centerX: options.centerX,
        css: {
          top: '30%',
          left: '50%',
          border: '0',
          padding: '0',
          backgroundColor: 'none',
          width: options.width
        },
        overlayCSS: {
          backgroundColor: options.overlayColor,
          opacity: options.opacity,
          cursor: 'wait',
          zIndex: '10'
        },
        onUnblock() {
          if (el && el[0]) {
            mUtil.css(el[0], 'position', '');
            mUtil.css(el[0], 'zoom', '');
          }
        }
      };

      if (target == 'body') {
        params.css.top = '50%';
        $.blockUI(params);
      } else {
        var el = $(target);
        el.block(params);
      }
    },

    /**
        * Un-blocks the blocked element
        * @param {object} target jQuery element object
        */
    unblock(target) {
      if (target && target != 'body') {
        $(target).unblock();
      } else {
        $.unblockUI();
      }
    },

    /**
        * Blocks the page body element with loading indicator
        * @param {object} options
        */
    blockPage(options) {
      return mApp.block('body', options);
    },

    /**
        * Un-blocks the blocked page body element
        */
    unblockPage() {
      return mApp.unblock('body');
    },

    /**
        * Enable loader progress for button and other elements
        * @param {object} target jQuery element object
        * @param {object} options
        */
    progress(target, options) {
      const skin = (options && options.skin) ? options.skin : 'light';
      const alignment = (options && options.alignment) ? options.alignment : 'right';
      const size = (options && options.size) ? `m-spinner--${options.size}` : '';
      const classes = `${'m-loader ' + 'm-loader--'}${skin} m-loader--${alignment} m-loader--${size}`;

      mApp.unprogress(target);

      $(target).addClass(classes);
      $(target).data('progress-classes', classes);
    },

    /**
        * Disable loader progress for button and other elements
        * @param {object} target jQuery element object
        */
    unprogress(target) {
      $(target).removeClass($(target).data('progress-classes'));
    },

    /**
        * Gets state color's hex code by color name
        * @param {string} name Color name
        * @returns {string}
        */
    getColor(name) {
      return colors[name];
    }
  };
}());

//= = Initialize mApp class on document ready
$(document).ready(() => {
  mApp.init({});
});
