/**
 * @class mUtil  Metronic base utilize class that privides helper functions
 */
//= = Polyfill
// matches polyfill
this.Element && (function (ElementPrototype) {
  ElementPrototype.matches = ElementPrototype.matches
        || ElementPrototype.matchesSelector
        || ElementPrototype.webkitMatchesSelector
        || ElementPrototype.msMatchesSelector
        || function (selector) {
          const node = this;


          const nodes = (node.parentNode || node.document).querySelectorAll(selector);


          let i = -1;
          while (nodes[++i] && nodes[i] != node);
          return !!nodes[i];
        };
}(Element.prototype));

// closest polyfill
this.Element && (function (ElementPrototype) {
  ElementPrototype.closest = ElementPrototype.closest
        || function (selector) {
          let el = this;
          while (el.matches && !el.matches(selector)) el = el.parentNode;
          return el.matches ? el : null;
        };
}(Element.prototype));


// matches polyfill
this.Element && (function (ElementPrototype) {
  ElementPrototype.matches = ElementPrototype.matches
        || ElementPrototype.matchesSelector
        || ElementPrototype.webkitMatchesSelector
        || ElementPrototype.msMatchesSelector
        || function (selector) {
          const node = this;


          const nodes = (node.parentNode || node.document).querySelectorAll(selector);


          let i = -1;
          while (nodes[++i] && nodes[i] != node);
          return !!nodes[i];
        };
}(Element.prototype));

//
// requestAnimationFrame polyfill by Erik Möller.
//  With fixes from Paul Irish and Tino Zijdel
//
//  http://paulirish.com/2011/requestanimationframe-for-smart-animating/
//  http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
//
//  MIT license
//
(function () {
  let lastTime = 0;
  const vendors = ['webkit', 'moz'];
  for (let x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[`${vendors[x]}RequestAnimationFrame`];
    window.cancelAnimationFrame = window[`${vendors[x]}CancelAnimationFrame`] || window[`${vendors[x]}CancelRequestAnimationFrame`];
  }

  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = function (callback) {
      const currTime = new Date().getTime();
      const timeToCall = Math.max(0, 16 - (currTime - lastTime));
      const id = window.setTimeout(() => {
        callback(currTime + timeToCall);
      }, timeToCall);
      lastTime = currTime + timeToCall;
      return id;
    };
  }

  if (!window.cancelAnimationFrame) {
    window.cancelAnimationFrame = function (id) {
      clearTimeout(id);
    };
  }
}());

// Source: https://github.com/jserz/js_piece/blob/master/DOM/ParentNode/prepend()/prepend().md
(function (arr) {
  arr.forEach((item) => {
    if (item.hasOwnProperty('prepend')) {
      return;
    }
    Object.defineProperty(item, 'prepend', {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function prepend() {
        const argArr = Array.prototype.slice.call(arguments);


        const docFrag = document.createDocumentFragment();

        argArr.forEach((argItem) => {
          const isNode = argItem instanceof Node;
          docFrag.appendChild(isNode ? argItem : document.createTextNode(String(argItem)));
        });

        this.insertBefore(docFrag, this.firstChild);
      }
    });
  });
}([Element.prototype, Document.prototype, DocumentFragment.prototype]));

//= = Global variables
window.mUtilElementDataStore = {};
window.mUtilElementDataStoreID = 0;
window.mUtilDelegatedEventHandlers = {};

var mUtil = (function () {
  const resizeHandlers = [];

  /** @type {object} breakpoints The device width breakpoints * */
  let breakpoints = {
    sm: 544, // Small screen / phone
    md: 768, // Medium screen / tablet
    lg: 1024, // Large screen / desktop
    xl: 1200 // Extra large screen / wide desktop
  };

  /**
     * Handle window resize event with some
     * delay to attach event handlers upon resize complete
     */
  const _windowResizeHandler = function () {
    const _runResizeHandlers = function () {
      // reinitialize other subscribed elements
      for (let i = 0; i < resizeHandlers.length; i++) {
        const each = resizeHandlers[i];
        each.call();
      }
    };

    let timeout = false; // holder for timeout id
    const delay = 250; // delay after event is "complete" to run callback

    window.addEventListener('resize', () => {
      clearTimeout(timeout);
      timeout = setTimeout(() => {
        _runResizeHandlers();
      }, delay); // wait 50ms until window resize finishes.
    });
  };

  return {
    /**
         * Class main initializer.
         * @param {object} options.
         * @returns null
         */
    // main function to initiate the theme
    init(options) {
      if (options && options.breakpoints) {
        breakpoints = options.breakpoints;
      }

      _windowResizeHandler();
    },

    /**
         * Adds window resize event handler.
         * @param {function} callback function.
         */
    addResizeHandler(callback) {
      resizeHandlers.push(callback);
    },

    /**
         * Removes window resize event handler.
         * @param {function} callback function.
         */
    removeResizeHandler(callback) {
      for (let i = 0; i < resizeHandlers.length; i++) {
        if (callback === resizeHandlers[i]) {
          delete resizeHandlers[i];
        }
      }
    },

    /**
         * Trigger window resize handlers.
         */
    runResizeHandlers() {
      _runResizeHandlers();
    },

    resize() {
      if (typeof (Event) === 'function') {
        // modern browsers
        window.dispatchEvent(new Event('resize'));
      } else {
        // for IE and other old browsers
        // causes deprecation warning on modern browsers
        const evt = window.document.createEvent('UIEvents');
        evt.initUIEvent('resize', true, false, window, 0);
        window.dispatchEvent(evt);
      }
    },

    /**
         * Get GET parameter value from URL.
         * @param {string} paramName Parameter name.
         * @returns {string}
         */
    getURLParam(paramName) {
      const searchString = window.location.search.substring(1);


      let i; let val; const
        params = searchString.split('&');

      for (i = 0; i < params.length; i++) {
        val = params[i].split('=');
        if (val[0] == paramName) {
          return unescape(val[1]);
        }
      }

      return null;
    },

    /**
         * Checks whether current device is mobile touch.
         * @returns {boolean}
         */
    isMobileDevice() {
      return (this.getViewPort().width < this.getBreakpoint('lg'));
    },

    /**
         * Checks whether current device is desktop.
         * @returns {boolean}
         */
    isDesktopDevice() {
      return !mUtil.isMobileDevice();
    },

    /**
         * Gets browser window viewport size. Ref:
         * http://andylangton.co.uk/articles/javascript/get-viewport-size-javascript/
         * @returns {object}
         */
    getViewPort() {
      let e = window;


      let a = 'inner';
      if (!('innerWidth' in window)) {
        a = 'client';
        e = document.documentElement || document.body;
      }

      return {
        width: e[`${a}Width`],
        height: e[`${a}Height`]
      };
    },

    /**
         * Checks whether given device mode is currently activated.
         * @param {string} mode Responsive mode name(e.g: desktop,
         *     desktop-and-tablet, tablet, tablet-and-mobile, mobile)
         * @returns {boolean}
         */
    isInResponsiveRange(mode) {
      let breakpoint = this.getViewPort().width;

      if (mode == 'general') {
        return true;
      } if (mode == 'desktop' && breakpoint >= (this.getBreakpoint('lg') + 1)) {
        return true;
      } if (mode == 'tablet' && (breakpoint >= (this.getBreakpoint('md') + 1) && breakpoint < this.getBreakpoint('lg'))) {
        return true;
      } if (mode == 'mobile' && breakpoint <= this.getBreakpoint('md')) {
        return true;
      } if (mode == 'desktop-and-tablet' && breakpoint >= (this.getBreakpoint('md') + 1)) {
        return true;
      } if (mode == 'tablet-and-mobile' && breakpoint <= this.getBreakpoint('lg')) {
        return true;
      } if (mode == 'minimal-desktop-and-below' && breakpoint <= this.getBreakpoint('xl')) {
        return true;
      }

      return false;
    },

    /**
         * Generates unique ID for give prefix.
         * @param {string} prefix Prefix for generated ID
         * @returns {boolean}
         */
    getUniqueID(prefix) {
      return prefix + Math.floor(Math.random() * (new Date()).getTime());
    },

    /**
         * Gets window width for give breakpoint mode.
         * @param {string} mode Responsive mode name(e.g: xl, lg, md, sm)
         * @returns {number}
         */
    getBreakpoint(mode) {
      return breakpoints[mode];
    },

    /**
         * Checks whether object has property matchs given key path.
         * @param {object} obj Object contains values paired with given key path
         * @param {string} keys Keys path seperated with dots
         * @returns {object}
         */
    isset(obj, keys) {
      let stone;

      keys = keys || '';

      if (keys.indexOf('[') !== -1) {
        throw new Error('Unsupported object path notation.');
      }

      keys = keys.split('.');

      do {
        if (obj === undefined) {
          return false;
        }

        stone = keys.shift();

        if (!obj.hasOwnProperty(stone)) {
          return false;
        }

        obj = obj[stone];
      } while (keys.length);

      return true;
    },

    /**
         * Gets highest z-index of the given element parents
         * @param {object} el jQuery element object
         * @returns {number}
         */
    getHighestZindex(el) {
      let elem = mUtil.get(el);


      let position; let
        value;

      while (elem && elem !== document) {
        // Ignore z-index if position is set to a value where z-index is ignored by the browser
        // This makes behavior of this function consistent across browsers
        // WebKit always returns auto if the element is positioned
        position = mUtil.css(elem, 'position');

        if (position === 'absolute' || position === 'relative' || position === 'fixed') {
          // IE returns 0 when zIndex is not specified
          // other browsers return a string
          // we ignore the case of nested elements with an explicit value of 0
          // <div style="z-index: -10;"><div style="z-index: 0;"></div></div>
          value = parseInt(mUtil.css(elem, 'z-index'));

          if (!isNaN(value) && value !== 0) {
            return value;
          }
        }

        elem = elem.parentNode;
      }

      return null;
    },

    /**
         * Checks whether the element has any parent with fixed positionfreg
         * @param {object} el jQuery element object
         * @returns {boolean}
         */
    hasFixedPositionedParent(el) {
      while (el && el !== document) {
        position = mUtil.css(el, 'position');

        if (position === 'fixed') {
          return true;
        }

        el = el.parentNode;
      }

      return false;
    },

    /**
         * Simulates delay
         */
    sleep(milliseconds) {
      const start = new Date().getTime();
      for (let i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
          break;
        }
      }
    },

    /**
         * Gets randomly generated integer value within given min and max range
         * @param {number} min Range start value
         * @param {number} min Range end value
         * @returns {number}
         */
    getRandomInt(min, max) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    },

    /**
         * Checks whether Angular library is included
         * @returns {boolean}
         */
    isAngularVersion() {
      return window.Zone !== undefined;
    },

    //= = jQuery Workarounds

    //= = Deep extend:  $.extend(true, {}, objA, objB);
    deepExtend(out) {
      out = out || {};

      for (let i = 1; i < arguments.length; i++) {
        const obj = arguments[i];

        if (!obj) { continue; }

        for (const key in obj) {
          if (obj.hasOwnProperty(key)) {
            if (typeof obj[key] === 'object') { out[key] = mUtil.deepExtend(out[key], obj[key]); } else { out[key] = obj[key]; }
          }
        }
      }

      return out;
    },

    //= = extend:  $.extend({}, objA, objB);
    extend(out) {
      out = out || {};

      for (let i = 1; i < arguments.length; i++) {
        if (!arguments[i]) { continue; }

        for (const key in arguments[i]) {
          if (arguments[i].hasOwnProperty(key)) { out[key] = arguments[i][key]; }
        }
      }

      return out;
    },

    get(query) {
      let el;

      if (query === document) {
        return document;
      }

      if (query && query.nodeType === 1) {
        return query;
      }

      if (el = document.getElementById(query)) {
        return el;
      } if (el = document.getElementsByTagName(query)) {
        return el[0];
      } if (el = document.getElementsByClassName(query)) {
        return el[0];
      }
      return null;
    },

    getByClass(query) {
      let el;

      if (el = document.getElementsByClassName(query)) {
        return el[0];
      }
      return null;
    },

    /**
         * Checks whether the element has given classes
         * @param {object} el jQuery element object
         * @param {string} Classes string
         * @returns {boolean}
         */
    hasClasses(el, classes) {
      if (!el) {
        return;
      }

      const classesArr = classes.split(' ');

      for (let i = 0; i < classesArr.length; i++) {
        if (mUtil.hasClass(el, mUtil.trim(classesArr[i])) == false) {
          return false;
        }
      }

      return true;
    },

    hasClass(el, className) {
      if (!el) {
        return;
      }

      return el.classList ? el.classList.contains(className) : new RegExp(`\\b${className}\\b`).test(el.className);
    },

    addClass(el, className) {
      if (!el || typeof className === 'undefined') {
        return;
      }

      const classNames = className.split(' ');

      if (el.classList) {
        for (var i = 0; i < classNames.length; i++) {
          if (classNames[i] && classNames[i].length > 0) {
            el.classList.add(mUtil.trim(classNames[i]));
          }
        }
      } else if (!mUtil.hasClass(el, className)) {
        for (var i = 0; i < classNames.length; i++) {
          el.className += ` ${mUtil.trim(classNames[i])}`;
        }
      }
    },

    removeClass(el, className) {
      if (!el || typeof className === 'undefined') {
        return;
      }

      const classNames = className.split(' ');

      if (el.classList) {
        for (var i = 0; i < classNames.length; i++) {
          el.classList.remove(mUtil.trim(classNames[i]));
        }
      } else if (mUtil.hasClass(el, className)) {
        for (var i = 0; i < classNames.length; i++) {
          el.className = el.className.replace(new RegExp(`\\b${mUtil.trim(classNames[i])}\\b`, 'g'), '');
        }
      }
    },

    triggerCustomEvent(el, eventName, data) {
      if (window.CustomEvent) {
        var event = new CustomEvent(eventName, {
          detail: data
        });
      } else {
        var event = document.createEvent('CustomEvent');
        event.initCustomEvent(eventName, true, true, data);
      }

      el.dispatchEvent(event);
    },

    trim(string) {
      return string.trim();
    },

    eventTriggered(e) {
      if (e.currentTarget.dataset.triggered) {
        return true;
      }
      e.currentTarget.dataset.triggered = true;

      return false;
    },

    remove(el) {
      if (el && el.parentNode) {
        el.parentNode.removeChild(el);
      }
    },

    find(parent, query) {
      parent = mUtil.get(parent);
      if (parent) {
        return parent.querySelector(query);
      }
    },

    findAll(parent, query) {
      parent = mUtil.get(parent);
      if (parent) {
        return parent.querySelectorAll(query);
      }
    },

    insertAfter(el, referenceNode) {
      return referenceNode.parentNode.insertBefore(el, referenceNode.nextSibling);
    },

    parents(el, query) {
      function collectionHas(a, b) { // helper function (see below)
        for (let i = 0, len = a.length; i < len; i++) {
          if (a[i] == b) return true;
        }

        return false;
      }

      function findParentBySelector(el, selector) {
        const all = document.querySelectorAll(selector);
        let cur = el.parentNode;

        while (cur && !collectionHas(all, cur)) { // keep going up until you find a match
          cur = cur.parentNode; // go up
        }

        return cur; // will return null if not found
      }

      return findParentBySelector(el, query);
    },

    children(el, selector, log) {
      if (!el || !el.childNodes) {
        return;
      }

      const result = [];


      var i = 0;


      const l = el.childNodes.length;

      for (var i; i < l; ++i) {
        if (el.childNodes[i].nodeType == 1 && mUtil.matches(el.childNodes[i], selector, log)) {
          result.push(el.childNodes[i]);
        }
      }

      return result;
    },

    child(el, selector, log) {
      const children = mUtil.children(el, selector, log);

      return children ? children[0] : null;
    },

    matches(el, selector, log) {
      const p = Element.prototype;
      const f = p.matches || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || function (s) {
        return [].indexOf.call(document.querySelectorAll(s), this) !== -1;
      };

      if (el && el.tagName) {
        return f.call(el, selector);
      }
      return false;
    },

    data(element) {
      element = mUtil.get(element);

      return {
        set(name, data) {
          if (element.customDataTag === undefined) {
            mUtilElementDataStoreID++;
            element.customDataTag = mUtilElementDataStoreID;
          }

          if (mUtilElementDataStore[element.customDataTag] === undefined) {
            mUtilElementDataStore[element.customDataTag] = {};
          }

          mUtilElementDataStore[element.customDataTag][name] = data;
        },

        get(name) {
          return this.has(name) ? mUtilElementDataStore[element.customDataTag][name] : null;
        },

        has(name) {
          return !!((mUtilElementDataStore[element.customDataTag] && mUtilElementDataStore[element.customDataTag][name]));
        },

        remove(name) {
          if (this.has(name)) {
            delete mUtilElementDataStore[element.customDataTag][name];
          }
        }
      };
    },

    outerWidth(el, margin) {
      var width;

      if (margin === true) {
        var width = parseFloat(el.offsetWidth);
        width += parseFloat(mUtil.css(el, 'margin-left')) + parseFloat(mUtil.css(el, 'margin-right'));

        return parseFloat(width);
      }
      var width = parseFloat(el.offsetWidth);

      return width;
    },

    offset(elem) {
      let rect; let
        win;
      elem = mUtil.get(elem);

      if (!elem) {
        return;
      }

      // Return zeros for disconnected and hidden (display: none) elements (gh-2310)
      // Support: IE <=11 only
      // Running getBoundingClientRect on a
      // disconnected node in IE throws an error

      if (!elem.getClientRects().length) {
        return { top: 0, left: 0 };
      }

      // Get document-relative position by adding viewport scroll to viewport-relative gBCR
      rect = elem.getBoundingClientRect();
      win = elem.ownerDocument.defaultView;

      return {
        top: rect.top + win.pageYOffset,
        left: rect.left + win.pageXOffset
      };
    },

    height(el) {
      return mUtil.css(el, 'height');
    },

    visible(el) {
      return !(el.offsetWidth === 0 && el.offsetHeight === 0);
    },

    attr(el, name, value) {
      el = mUtil.get(el);

      if (el == undefined) {
        return;
      }

      if (value !== undefined) {
        el.setAttribute(name, value);
      } else {
        return el.getAttribute(name);
      }
    },

    hasAttr(el, name) {
      el = mUtil.get(el);

      if (el == undefined) {
        return;
      }

      return !!el.getAttribute(name);
    },

    removeAttr(el, name) {
      el = mUtil.get(el);

      if (el == undefined) {
        return;
      }

      el.removeAttribute(name);
    },

    animate(from, to, duration, update, easing, done) {
      /**
             * TinyAnimate.easings
             *  Adapted from jQuery Easing
             */
      const easings = {};
      var easing;

      easings.linear = function (t, b, c, d) {
        return c * t / d + b;
      };

      easing = easings.linear;

      // Early bail out if called incorrectly
      if (typeof from !== 'number'
                || typeof to !== 'number'
                || typeof duration !== 'number'
                || typeof update !== 'function') {
        return;
      }

      // Create mock done() function if necessary
      if (typeof done !== 'function') {
        done = function () {};
      }

      // Pick implementation (requestAnimationFrame | setTimeout)
      const rAF = window.requestAnimationFrame || function (callback) {
        window.setTimeout(callback, 1000 / 50);
      };

      // Animation loop
      const canceled = false;
      const change = to - from;

      function loop(timestamp) {
        const time = (timestamp || +new Date()) - start;

        if (time >= 0) {
          update(easing(time, from, change, duration));
        }
        if (time >= 0 && time >= duration) {
          update(to);
          done();
        } else {
          rAF(loop);
        }
      }

      update(from);

      // Start animation loop
      var start = window.performance && window.performance.now ? window.performance.now() : +new Date();

      rAF(loop);
    },

    actualCss(el, prop, cache) {
      if (el instanceof HTMLElement === false) {
        return;
      }

      if (!el.getAttribute(`m-hidden-${prop}`) || cache === false) {
        let value;

        // the element is hidden so:
        // making the el block so we can meassure its height but still be hidden
        el.style.cssText = 'position: absolute; visibility: hidden; display: block;';

        if (prop == 'width') {
          value = el.offsetWidth;
        } else if (prop == 'height') {
          value = el.offsetHeight;
        }

        el.style.cssText = '';

        // store it in cache
        el.setAttribute(`m-hidden-${prop}`, value);

        return parseFloat(value);
      }
      // store it in cache
      return parseFloat(el.getAttribute(`m-hidden-${prop}`));
    },

    actualHeight(el, cache) {
      return mUtil.actualCss(el, 'height', cache);
    },

    actualWidth(el, cache) {
      return mUtil.actualCss(el, 'width', cache);
    },

    getScroll(element, method) {
      // The passed in `method` value should be 'Top' or 'Left'
      method = `scroll${method}`;
      return (element == window || element == document) ? (
        self[(method == 'scrollTop') ? 'pageYOffset' : 'pageXOffset']
                || (browserSupportsBoxModel && document.documentElement[method])
                || document.body[method]
      ) : element[method];
    },

    css(el, styleProp, value) {
      el = mUtil.get(el);

      if (!el) {
        return;
      }

      if (value !== undefined) {
        el.style[styleProp] = value;
      } else {
        var value; const
          defaultView = (el.ownerDocument || document).defaultView;
        // W3C standard way:
        if (defaultView && defaultView.getComputedStyle) {
          // sanitize property name to css notation
          // (hyphen separated words eg. font-Size)
          styleProp = styleProp.replace(/([A-Z])/g, '-$1').toLowerCase();
          return defaultView.getComputedStyle(el, null).getPropertyValue(styleProp);
        } if (el.currentStyle) { // IE
          // sanitize property name to camelCase
          styleProp = styleProp.replace(/\-(\w)/g, (str, letter) => letter.toUpperCase());
          value = el.currentStyle[styleProp];
          // convert other units to pixels on IE
          if (/^\d+(em|pt|%|ex)?$/i.test(value)) {
            return (function (value) {
              const oldLeft = el.style.left;


              const oldRsLeft = el.runtimeStyle.left;
              el.runtimeStyle.left = el.currentStyle.left;
              el.style.left = value || 0;
              value = `${el.style.pixelLeft}px`;
              el.style.left = oldLeft;
              el.runtimeStyle.left = oldRsLeft;
              return value;
            }(value));
          }
          return value;
        }
      }
    },

    slide(el, dir, speed, callback, recalcMaxHeight) {
      if (!el || (dir == 'up' && mUtil.visible(el) === false) || (dir == 'down' && mUtil.visible(el) === true)) {
        return;
      }

      speed = (speed || 600);
      const calcHeight = mUtil.actualHeight(el);
      let calcPaddingTop = false;
      let calcPaddingBottom = false;

      if (mUtil.css(el, 'padding-top') && mUtil.data(el).has('slide-padding-top') !== true) {
        mUtil.data(el).set('slide-padding-top', mUtil.css(el, 'padding-top'));
      }

      if (mUtil.css(el, 'padding-bottom') && mUtil.data(el).has('slide-padding-bottom') !== true) {
        mUtil.data(el).set('slide-padding-bottom', mUtil.css(el, 'padding-bottom'));
      }

      if (mUtil.data(el).has('slide-padding-top')) {
        calcPaddingTop = parseInt(mUtil.data(el).get('slide-padding-top'));
      }

      if (mUtil.data(el).has('slide-padding-bottom')) {
        calcPaddingBottom = parseInt(mUtil.data(el).get('slide-padding-bottom'));
      }

      if (dir == 'up') { // up
        el.style.cssText = 'display: block; overflow: hidden;';

        if (calcPaddingTop) {
          mUtil.animate(0, calcPaddingTop, speed, (value) => {
            el.style.paddingTop = `${calcPaddingTop - value}px`;
          }, 'linear');
        }

        if (calcPaddingBottom) {
          mUtil.animate(0, calcPaddingBottom, speed, (value) => {
            el.style.paddingBottom = `${calcPaddingBottom - value}px`;
          }, 'linear');
        }

        mUtil.animate(0, calcHeight, speed, (value) => {
          el.style.height = `${calcHeight - value}px`;
        }, 'linear', () => {
          callback();
          el.style.height = '';
          el.style.display = 'none';
        });
      } else if (dir == 'down') { // down
        el.style.cssText = 'display: block; overflow: hidden;';

        if (calcPaddingTop) {
          mUtil.animate(0, calcPaddingTop, speed, (value) => {
            el.style.paddingTop = `${value}px`;
          }, 'linear', () => {
            el.style.paddingTop = '';
          });
        }

        if (calcPaddingBottom) {
          mUtil.animate(0, calcPaddingBottom, speed, (value) => {
            el.style.paddingBottom = `${value}px`;
          }, 'linear', () => {
            el.style.paddingBottom = '';
          });
        }

        mUtil.animate(0, calcHeight, speed, (value) => {
          el.style.height = `${value}px`;
        }, 'linear', () => {
          callback();
          el.style.height = '';
          el.style.display = '';
          el.style.overflow = '';
        });
      }
    },

    slideUp(el, speed, callback) {
      mUtil.slide(el, 'up', speed, callback);
    },

    slideDown(el, speed, callback) {
      mUtil.slide(el, 'down', speed, callback);
    },

    show(el, display) {
      el.style.display = (display || 'block');
    },

    hide(el) {
      el.style.display = 'none';
    },

    addEvent(el, type, handler, one) {
      el = mUtil.get(el);
      if (typeof el !== 'undefined') {
        el.addEventListener(type, handler);
      }
    },

    removeEvent(el, type, handler) {
      el = mUtil.get(el);
      el.removeEventListener(type, handler);
    },

    on(element, selector, event, handler) {
      if (!selector) {
        return;
      }

      const eventId = mUtil.getUniqueID('event');

      mUtilDelegatedEventHandlers[eventId] = function (e) {
        const targets = element.querySelectorAll(selector);
        let target = e.target;

        while (target && target !== element) {
          for (let i = 0, j = targets.length; i < j; i++) {
            if (target === targets[i]) {
              handler.call(target, e);
            }
          }

          target = target.parentNode;
        }
      };

      mUtil.addEvent(element, event, mUtilDelegatedEventHandlers[eventId]);

      return eventId;
    },

    off(element, event, eventId) {
      if (!element || !mUtilDelegatedEventHandlers[eventId]) {
        return;
      }

      mUtil.removeEvent(element, event, mUtilDelegatedEventHandlers[eventId]);

      delete mUtilDelegatedEventHandlers[eventId];
    },

    one: function onetime(el, type, callback) {
      el = mUtil.get(el);

      el.addEventListener(type, function (e) {
        // remove event
        e.target.removeEventListener(e.type, arguments.callee);
        // call handler
        return callback(e);
      });
    },

    hash(str) {
      let hash = 0;


      let i; let
        chr;

      if (str.length === 0) return hash;
      for (i = 0; i < str.length; i++) {
        chr = str.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
      }

      return hash;
    },

    animateClass(el, animationName, callback) {
      const animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

      mUtil.addClass(el, `animated ${animationName}`);

      mUtil.one(el, animationEnd, () => {
        mUtil.removeClass(el, `animated ${animationName}`);
      });

      if (callback) {
        mUtil.one(el.animationEnd, callback);
      }
    },

    animateDelay(el, value) {
      const vendors = ['webkit-', 'moz-', 'ms-', 'o-', ''];
      for (let i = 0; i < vendors.length; i++) {
        mUtil.css(el, `${vendors[i]}animation-delay`, value);
      }
    },

    animateDuration(el, value) {
      const vendors = ['webkit-', 'moz-', 'ms-', 'o-', ''];
      for (let i = 0; i < vendors.length; i++) {
        mUtil.css(el, `${vendors[i]}animation-duration`, value);
      }
    },

    scrollTo(target, offset, duration) {
      var duration = duration || 500;
      var target = mUtil.get(target);
      const targetPos = target ? mUtil.offset(target).top : 0;
      const scrollPos = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
      let from; let
        to;

      if (targetPos > scrollPos) {
        from = targetPos;
        to = scrollPos;
      } else {
        from = scrollPos;
        to = targetPos;
      }

      if (offset) {
        to += offset;
      }

      mUtil.animate(from, to, duration, (value) => {
        document.documentElement.scrollTop = value;
        document.body.parentNode.scrollTop = value;
        document.body.scrollTop = value;
      }); // , easing, done
    },

    scrollTop(offset, duration) {
      mUtil.scrollTo(null, offset, duration);
    },

    isArray(obj) {
      return obj && Array.isArray(obj);
    },

    ready(callback) {
      if (document.attachEvent ? document.readyState === 'complete' : document.readyState !== 'loading') {
        callback();
      } else {
        document.addEventListener('DOMContentLoaded', callback);
      }
    },

    isEmpty(obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          return false;
        }
      }

      return true;
    },

    numberString(nStr) {
      nStr += '';
      const x = nStr.split('.');
      let x1 = x[0];
      const x2 = x.length > 1 ? `.${x[1]}` : '';
      const rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
      }
      return x1 + x2;
    },

    detectIE() {
      const ua = window.navigator.userAgent;

      // Test values; Uncomment to check result …

      // IE 10
      // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

      // IE 11
      // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

      // Edge 12 (Spartan)
      // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

      // Edge 13
      // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

      const msie = ua.indexOf('MSIE ');
      if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
      }

      const trident = ua.indexOf('Trident/');
      if (trident > 0) {
        // IE 11 => return version number
        const rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
      }

      const edge = ua.indexOf('Edge/');
      if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
      }

      // other browser
      return false;
    },

    isRTL() {
      return (mUtil.attr(mUtil.get('html'), 'direction') == 'rtl');
    },

    //= = Scroller
    scrollerInit(element, options) {
      //= = Define init function
      function init() {
        let ps;
        let height;

        if (options.height instanceof Function) {
          height = parseInt(options.height.call());
        } else {
          height = parseInt(options.height);
        }

        //= = Destroy scroll on table and mobile modes
        if (options.disableForMobile && mUtil.isInResponsiveRange('tablet-and-mobile')) {
          if (ps = mUtil.data(element).get('ps')) {
            if (options.resetHeightOnDestroy) {
              mUtil.css(element, 'height', 'auto');
            } else {
              mUtil.css(element, 'overflow', 'auto');
              if (height > 0) {
                mUtil.css(element, 'height', `${height}px`);
              }
            }

            ps.destroy();
            ps = mUtil.data(element).remove('ps');
          } else if (height > 0) {
            mUtil.css(element, 'overflow', 'auto');
            mUtil.css(element, 'height', `${height}px`);
          }

          return;
        }

        if (height > 0) {
          mUtil.css(element, 'height', `${height}px`);
        }

        mUtil.css(element, 'overflow', 'hidden');

        //= = Init scroll
        if (ps = mUtil.data(element).get('ps')) {
          ps.update();
        } else {
          mUtil.addClass(element, 'm-scroller');
          ps = new PerfectScrollbar(element, {
            wheelSpeed: 0.5,
            swipeEasing: true,
            wheelPropagation: false,
            minScrollbarLength: 40,
            suppressScrollX: !mUtil.isRTL()
          });

          mUtil.data(element).set('ps', ps);
        }
      }

      //= = Init
      init();

      //= = Handle window resize
      if (options.handleWindowResize) {
        mUtil.addResizeHandler(() => {
          init();
        });
      }
    },

    scrollerUpdate(element) {
      let ps;
      if (ps = mUtil.data(element).get('ps')) {
        ps.update();
      }
    },

    scrollersUpdate(parent) {
      const scrollers = mUtil.findAll(parent, '.ps');
      for (let i = 0, len = scrollers.length; i < len; i++) {
        mUtil.scrollerUpdate(scrollers[i]);
      }
    },

    scrollerTop(element) {
      let ps;
      if (ps = mUtil.data(element).get('ps')) {
        element.scrollTop = 0;
      }
    },

    scrollerDestroy(element) {
      let ps;
      if (ps = mUtil.data(element).get('ps')) {
        ps.destroy();
        ps = mUtil.data(element).remove('ps');
      }
    }
  };
}());

//= = Initialize mUtil class on document ready
mUtil.ready(() => {
  mUtil.init();
});
