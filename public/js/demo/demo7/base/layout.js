const mLayout = (function () {
  let horMenu;
  let asideMenu;
  let asideMenuOffcanvas;
  let horMenuOffcanvas;

  //= = Header
  const initStickyHeader = function () {
    let tmp;
    const headerEl = mUtil.get('m_header');
    const options = {
      offset: {},
      minimize: {}
    };

    if (mUtil.attr(headerEl, 'm-minimize-mobile') == 'hide') {
      options.minimize.mobile = {};
      options.minimize.mobile.on = 'm-header--hide';
      options.minimize.mobile.off = 'm-header--show';
    } else {
      options.minimize.mobile = false;
    }

    if (mUtil.attr(headerEl, 'm-minimize') == 'hide') {
      options.minimize.desktop = {};
      options.minimize.desktop.on = 'm-header--hide';
      options.minimize.desktop.off = 'm-header--show';
    } else {
      options.minimize.desktop = false;
    }

    if (tmp = mUtil.attr(headerEl, 'm-minimize-offset')) {
      options.offset.desktop = tmp;
    }

    if (tmp = mUtil.attr(headerEl, 'm-minimize-mobile-offset')) {
      options.offset.mobile = tmp;
    }

    header = new mHeader('m_header', options);
  };

  //= = Hor menu
  const initHorMenu = function () {
    // init aside left offcanvas
    horMenuOffcanvas = new mOffcanvas('m_header_menu', {
      overlay: true,
      baseClass: 'm-aside-header-menu-mobile',
      closeBy: 'm_aside_header_menu_mobile_close_btn',
      toggleBy: {
        target: 'm_aside_header_menu_mobile_toggle',
        state: 'm-brand__toggler--active'
      }
    });

    horMenu = new mMenu('m_header_menu', {
      submenu: {
        desktop: 'dropdown',
        tablet: 'accordion',
        mobile: 'accordion'
      },
      accordion: {
        slideSpeed: 200, // accordion toggle slide speed in milliseconds
        expandAll: false // allow having multiple expanded accordions in the menu
      }
    });
  };

  // handle vertical menu
  const initLeftAsideMenu = function () {
    //= = Init aside menu
    const menu = mUtil.get('m_ver_menu');
    const menuDesktopMode = (mUtil.attr(menu, 'm-menu-dropdown') === '1' ? 'dropdown' : 'accordion');

    asideMenu = new mMenu('m_ver_menu', {
      // submenu setup
      submenu: {
        desktop: {
          // by default the menu mode set to accordion in desktop mode
          default: menuDesktopMode,
          // whenever body has this class switch the menu mode to dropdown
          state: {
            body: 'm-aside-left--minimize',
            mode: 'dropdown'
          }
        },
        tablet: 'accordion', // menu set to accordion in tablet mode
        mobile: 'accordion' // menu set to accordion in mobile mode
      },

      // accordion setup
      accordion: {
        autoScroll: false, // enable auto scrolling(focus) to the clicked menu item
        expandAll: false // allow having multiple expanded accordions in the menu
      }
    });
  };

  // handle vertical menu
  const initLeftAside = function () {
    // init aside left offcanvas
    const asideLeft = mUtil.get('m_aside_left');
    const asideOffcanvasClass = mUtil.hasClass(asideLeft, 'm-aside-left--offcanvas-default') ? 'm-aside-left--offcanvas-default' : 'm-aside-left';

    asideMenuOffcanvas = new mOffcanvas('m_aside_left', {
      baseClass: asideOffcanvasClass,
      overlay: true,
      closeBy: 'm_aside_left_close_btn',
      toggleBy: {
        target: 'm_aside_left_offcanvas_toggle',
        state: 'm-brand__toggler--active'
      }
    });

    //= = Handle full height dropdowns
    const query = mUtil.findAll(asideLeft, '.m-menu__item--submenu-fullheight .m-menu__submenu > .m-menu__wrapper');
    for (let i = 0, j = query.length; i < j; i++) {
      var item = query[i];

      mUtil.scrollerInit(item, {
        disableForMobile: true,
        resetHeightOnDestroy: true,
        handleWindowResize: true,
        height() {
          return mUtil.getViewPort().height;
        }
      });

      //= = Update scroller on submenu toggle
      asideMenu.on('submenuToggle', (menu, submenu) => {
        if (submenu && item && item.contains(submenu)) {
          mUtil.scrollerUpdate(item);
        }
      });
    }
  };

  //= = Topbar
  const initTopbar = function () {
    $('#m_aside_header_topbar_mobile_toggle').click(() => {
      $('body').toggleClass('m-topbar--on');
    });
  };

  //= = Quicksearch
  const initQuicksearch = function () {
    if ($('#m_quicksearch').length === 0) {
      return;
    }

    quicksearch = new mQuicksearch('m_quicksearch', {
      mode: mUtil.attr('m_quicksearch', 'm-quicksearch-mode'), // quick search type
      minLength: 1
    });

    // <div class="m-search-results m-search-results--skin-light"><span class="m-search-result__message">Something went wrong</div></div>

    quicksearch.on('search', (the) => {
      the.showProgress();

      $.ajax({
        url: 'inc/api/quick_search.php',
        data: { query: the.query },
        dataType: 'html',
        success(res) {
          the.hideProgress();
          the.showResult(res);
        },
        error(res) {
          the.hideProgress();
          the.showError('Connection error. Pleae try again later.');
        }
      });
    });
  };

  //= = Scrolltop
  const initScrollTop = function () {
    const scrollTop = new mScrollTop('m_scroll_top', {
      offset: 300,
      speed: 600
    });
  };


  return {
    init() {
      this.initHeader();
      this.initAside();
    },

    initHeader() {
      initStickyHeader();
      initHorMenu();
      initTopbar();
      initQuicksearch();
      initScrollTop();
    },

    initAside() {
      initLeftAsideMenu();
      initLeftAside();
    },

    getAsideMenu() {
      return asideMenu;
    },

    closeMobileAsideMenuOffcanvas() {
      if (mUtil.isMobileDevice()) {
        asideMenuOffcanvas.hide();
      }
    },

    closeMobileHorMenuOffcanvas() {
      if (mUtil.isMobileDevice()) {
        horMenuOffcanvas.hide();
      }
    }
  };
}());

$(document).ready(() => {
  if (mUtil.isAngularVersion() === false) {
    mLayout.init();
  }
});
