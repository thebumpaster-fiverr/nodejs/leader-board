const DatatablesAdvancedFooterCalllback = (function () {
  const initTable1 = function () {
    const table = $('#m_table_1');

    // begin first table
    table.DataTable({
      responsive: true,
      pageLength: 5,
      lengthMenu: [[2, 5, 10, 15, -1], [2, 5, 10, 15, 'All']],
      footerCallback(row, data, start, end, display) {
        const column = 6;
        const api = this.api(); var
          data;

        // Remove the formatting to get integer data for summation
        const intVal = function (i) {
          return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
        };

        // Total over all pages
        const total = api.column(column).data().reduce((a, b) => intVal(a) + intVal(b), 0);

        // Total over this page
        const pageTotal = api.column(column, { page: 'current' }).data().reduce((a, b) => intVal(a) + intVal(b), 0);

        // Update footer
        $(api.column(column).footer()).html(`$${mUtil.numberString(pageTotal.toFixed(2))}<br/> ( $${mUtil.numberString(total.toFixed(2))} total)`,);
      },
    });
  };

  return {

    // main function to initiate the module
    init() {
      initTable1();
    },

  };
}());

jQuery(document).ready(() => {
  DatatablesAdvancedFooterCalllback.init();
});
