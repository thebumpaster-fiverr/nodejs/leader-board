//= = Class definition

const BootstrapMarkdown = (function () {
  //= = Private functions
  const demos = function () {

  };

  return {
    // public functions
    init() {
      demos();
    }
  };
}());

//= = Initialization
jQuery(document).ready(() => {
  BootstrapMarkdown.init();
});
