//= = Class definition
const FormRepeater = (function () {
  //= = Private functions
  const demo1 = function () {
    $('#m_repeater_1').repeater({
      initEmpty: false,

      defaultValues: {
        'text-input': 'foo'
      },

      show() {
        $(this).slideDown();
      },

      hide(deleteElement) {
        $(this).slideUp(deleteElement);
      }
    });
  };

  const demo2 = function () {
    $('#m_repeater_2').repeater({
      initEmpty: false,

      defaultValues: {
        'text-input': 'foo'
      },

      show() {
        $(this).slideDown();
      },

      hide(deleteElement) {
        if (confirm('Are you sure you want to delete this element?')) {
          $(this).slideUp(deleteElement);
        }
      }
    });
  };


  const demo3 = function () {
    $('#m_repeater_3').repeater({
      initEmpty: false,

      defaultValues: {
        'text-input': 'foo'
      },

      show() {
        $(this).slideDown();
      },

      hide(deleteElement) {
        if (confirm('Are you sure you want to delete this element?')) {
          $(this).slideUp(deleteElement);
        }
      }
    });
  };

  const demo4 = function () {
    $('#m_repeater_4').repeater({
      initEmpty: false,

      defaultValues: {
        'text-input': 'foo'
      },

      show() {
        $(this).slideDown();
      },

      hide(deleteElement) {
        $(this).slideUp(deleteElement);
      }
    });
  };

  const demo5 = function () {
    $('#m_repeater_5').repeater({
      initEmpty: false,

      defaultValues: {
        'text-input': 'foo'
      },

      show() {
        $(this).slideDown();
      },

      hide(deleteElement) {
        $(this).slideUp(deleteElement);
      }
    });
  };

  const demo6 = function () {
    $('#m_repeater_6').repeater({
      initEmpty: false,

      defaultValues: {
        'text-input': 'foo'
      },

      show() {
        $(this).slideDown();
      },

      hide(deleteElement) {
        $(this).slideUp(deleteElement);
      }
    });
  };
  return {
    // public functions
    init() {
      demo1();
      demo2();
      demo3();
      demo4();
      demo5();
      demo6();
    }
  };
}());

jQuery(document).ready(() => {
  FormRepeater.init();
});
