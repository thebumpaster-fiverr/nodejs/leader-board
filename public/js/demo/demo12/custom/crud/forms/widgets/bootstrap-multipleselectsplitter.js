//= = Class definition

const BootstrapMultipleSelectsplitter = (function () {
  //= = Private functions
  const demos = function () {
    // minimum setup
    $('#m_multipleselectsplitter_1, #m_multipleselectsplitter_2').multiselectsplitter();
  };

  return {
    // public functions
    init() {
      demos();
    }
  };
}());

jQuery(document).ready(() => {
  BootstrapMultipleSelectsplitter.init();
});
