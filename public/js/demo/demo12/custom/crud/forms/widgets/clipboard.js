//= = Class definition

const ClipboardDemo = (function () {
  //= = Private functions
  const demos = function () {
    // basic example
    new Clipboard('[data-clipboard=true]').on('success', (e) => {
      e.clearSelection();
      alert('Copied!');
    });
  };

  return {
    // public functions
    init() {
      demos();
    }
  };
}());

jQuery(document).ready(() => {
  ClipboardDemo.init();
});
