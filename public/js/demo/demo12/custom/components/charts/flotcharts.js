//= = Class definition
const FlotchartsDemo = (function () {
  //= = Private functions

  const demo1 = function () {
    let data = [];
    const totalPoints = 250;

    // random data generator for plot charts

    function getRandomData() {
      if (data.length > 0) data = data.slice(1);
      // do a random walk
      while (data.length < totalPoints) {
        const prev = data.length > 0 ? data[data.length - 1] : 50;
        let y = prev + Math.random() * 10 - 5;
        if (y < 0) y = 0;
        if (y > 100) y = 100;
        data.push(y);
      }
      // zip the generated y values with the x values
      const res = [];
      for (let i = 0; i < data.length; ++i) {
        res.push([i, data[i]]);
      }

      return res;
    }

    const d1 = [];
    for (var i = 0; i < Math.PI * 2; i += 0.25) { d1.push([i, Math.sin(i)]); }

    const d2 = [];
    for (var i = 0; i < Math.PI * 2; i += 0.25) { d2.push([i, Math.cos(i)]); }

    const d3 = [];
    for (var i = 0; i < Math.PI * 2; i += 0.1) { d3.push([i, Math.tan(i)]); }

    $.plot($('#m_flotcharts_1'), [{
      label: 'sin(x)',
      data: d1,
      lines: {
        lineWidth: 1,
      },
      shadowSize: 0
    }, {
      label: 'cos(x)',
      data: d2,
      lines: {
        lineWidth: 1,
      },
      shadowSize: 0
    }, {
      label: 'tan(x)',
      data: d3,
      lines: {
        lineWidth: 1,
      },
      shadowSize: 0
    }], {
      series: {
        lines: {
          show: true,
        },
        points: {
          show: true,
          fill: true,
          radius: 3,
          lineWidth: 1
        }
      },
      xaxis: {
        tickColor: '#eee',
        ticks: [0, [Math.PI / 2, '\u03c0/2'],
          [Math.PI, '\u03c0'],
          [Math.PI * 3 / 2, '3\u03c0/2'],
          [Math.PI * 2, '2\u03c0']
        ]
      },
      yaxis: {
        tickColor: '#eee',
        ticks: 10,
        min: -2,
        max: 2
      },
      grid: {
        borderColor: '#eee',
        borderWidth: 1
      }
    });
  };

  const demo2 = function () {
    function randValue() {
      return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
    }
    const pageviews = [
      [1, randValue()],
      [2, randValue()],
      [3, 2 + randValue()],
      [4, 3 + randValue()],
      [5, 5 + randValue()],
      [6, 10 + randValue()],
      [7, 15 + randValue()],
      [8, 20 + randValue()],
      [9, 25 + randValue()],
      [10, 30 + randValue()],
      [11, 35 + randValue()],
      [12, 25 + randValue()],
      [13, 15 + randValue()],
      [14, 20 + randValue()],
      [15, 45 + randValue()],
      [16, 50 + randValue()],
      [17, 65 + randValue()],
      [18, 70 + randValue()],
      [19, 85 + randValue()],
      [20, 80 + randValue()],
      [21, 75 + randValue()],
      [22, 80 + randValue()],
      [23, 75 + randValue()],
      [24, 70 + randValue()],
      [25, 65 + randValue()],
      [26, 75 + randValue()],
      [27, 80 + randValue()],
      [28, 85 + randValue()],
      [29, 90 + randValue()],
      [30, 95 + randValue()]
    ];
    const visitors = [
      [1, randValue() - 5],
      [2, randValue() - 5],
      [3, randValue() - 5],
      [4, 6 + randValue()],
      [5, 5 + randValue()],
      [6, 20 + randValue()],
      [7, 25 + randValue()],
      [8, 36 + randValue()],
      [9, 26 + randValue()],
      [10, 38 + randValue()],
      [11, 39 + randValue()],
      [12, 50 + randValue()],
      [13, 51 + randValue()],
      [14, 12 + randValue()],
      [15, 13 + randValue()],
      [16, 14 + randValue()],
      [17, 15 + randValue()],
      [18, 15 + randValue()],
      [19, 16 + randValue()],
      [20, 17 + randValue()],
      [21, 18 + randValue()],
      [22, 19 + randValue()],
      [23, 20 + randValue()],
      [24, 21 + randValue()],
      [25, 14 + randValue()],
      [26, 24 + randValue()],
      [27, 25 + randValue()],
      [28, 26 + randValue()],
      [29, 27 + randValue()],
      [30, 31 + randValue()]
    ];

    const plot = $.plot($('#m_flotcharts_2'), [{
      data: pageviews,
      label: 'Unique Visits',
      lines: {
        lineWidth: 1,
      },
      shadowSize: 0

    }, {
      data: visitors,
      label: 'Page Views',
      lines: {
        lineWidth: 1,
      },
      shadowSize: 0
    }], {
      series: {
        lines: {
          show: true,
          lineWidth: 2,
          fill: true,
          fillColor: {
            colors: [{
              opacity: 0.05
            }, {
              opacity: 0.01
            }]
          }
        },
        points: {
          show: true,
          radius: 3,
          lineWidth: 1
        },
        shadowSize: 2
      },
      grid: {
        hoverable: true,
        clickable: true,
        tickColor: '#eee',
        borderColor: '#eee',
        borderWidth: 1
      },
      colors: ['#d12610', '#37b7f3', '#52e136'],
      xaxis: {
        ticks: 11,
        tickDecimals: 0,
        tickColor: '#eee',
      },
      yaxis: {
        ticks: 11,
        tickDecimals: 0,
        tickColor: '#eee',
      }
    });

    function showTooltip(x, y, contents) {
      $(`<div id="tooltip">${contents}</div>`).css({
        position: 'absolute',
        display: 'none',
        top: y + 5,
        left: x + 15,
        border: '1px solid #333',
        padding: '4px',
        color: '#fff',
        'border-radius': '3px',
        'background-color': '#333',
        opacity: 0.80
      }).appendTo('body').fadeIn(200);
    }

    let previousPoint = null;
    $('#chart_2').bind('plothover', (event, pos, item) => {
      $('#x').text(pos.x.toFixed(2));
      $('#y').text(pos.y.toFixed(2));

      if (item) {
        if (previousPoint != item.dataIndex) {
          previousPoint = item.dataIndex;

          $('#tooltip').remove();
          const x = item.datapoint[0].toFixed(2);


          const y = item.datapoint[1].toFixed(2);

          showTooltip(item.pageX, item.pageY, `${item.series.label} of ${x} = ${y}`);
        }
      } else {
        $('#tooltip').remove();
        previousPoint = null;
      }
    });
  };

  const demo3 = function () {
    const sin = [];


    const cos = [];
    for (let i = 0; i < 14; i += 0.1) {
      sin.push([i, Math.sin(i)]);
      cos.push([i, Math.cos(i)]);
    }

    plot = $.plot($('#m_flotcharts_3'), [{
      data: sin,
      label: 'sin(x) = -0.00',
      lines: {
        lineWidth: 1,
      },
      shadowSize: 0
    }, {
      data: cos,
      label: 'cos(x) = -0.00',
      lines: {
        lineWidth: 1,
      },
      shadowSize: 0
    }], {
      series: {
        lines: {
          show: true
        }
      },
      crosshair: {
        mode: 'x'
      },
      grid: {
        hoverable: true,
        autoHighlight: false,
        tickColor: '#eee',
        borderColor: '#eee',
        borderWidth: 1
      },
      yaxis: {
        min: -1.2,
        max: 1.2
      }
    });

    const legends = $('#m_flotcharts_3 .legendLabel');
    legends.each(function () {
      // fix the widths so they don't jump around
      $(this).css('width', $(this).width());
    });

    let updateLegendTimeout = null;
    let latestPosition = null;

    function updateLegend() {
      updateLegendTimeout = null;

      const pos = latestPosition;

      const axes = plot.getAxes();
      if (pos.x < axes.xaxis.min || pos.x > axes.xaxis.max || pos.y < axes.yaxis.min || pos.y > axes.yaxis.max) return;

      let i; let j; const
        dataset = plot.getData();
      for (i = 0; i < dataset.length; ++i) {
        const series = dataset[i];

        // find the nearest points, x-wise
        for (j = 0; j < series.data.length; ++j) { if (series.data[j][0] > pos.x) break; }

        // now interpolate
        var y; const p1 = series.data[j - 1];


        const p2 = series.data[j];

        if (p1 == null) y = p2[1];
        else if (p2 == null) y = p1[1];
        else y = p1[1] + (p2[1] - p1[1]) * (pos.x - p1[0]) / (p2[0] - p1[0]);

        legends.eq(i).text(series.label.replace(/=.*/, `= ${y.toFixed(2)}`));
      }
    }

    $('#m_flotcharts_3').bind('plothover', (event, pos, item) => {
      latestPosition = pos;
      if (!updateLegendTimeout) updateLegendTimeout = setTimeout(updateLegend, 50);
    });
  };

  const demo4 = function () {
    let data = [];
    const totalPoints = 250;

    // random data generator for plot charts

    function getRandomData() {
      if (data.length > 0) data = data.slice(1);
      // do a random walk
      while (data.length < totalPoints) {
        const prev = data.length > 0 ? data[data.length - 1] : 50;
        let y = prev + Math.random() * 10 - 5;
        if (y < 0) y = 0;
        if (y > 100) y = 100;
        data.push(y);
      }
      // zip the generated y values with the x values
      const res = [];
      for (let i = 0; i < data.length; ++i) {
        res.push([i, data[i]]);
      }

      return res;
    }

    // server load
    const options = {
      series: {
        shadowSize: 1
      },
      lines: {
        show: true,
        lineWidth: 0.5,
        fill: true,
        fillColor: {
          colors: [{
            opacity: 0.1
          }, {
            opacity: 1
          }]
        }
      },
      yaxis: {
        min: 0,
        max: 100,
        tickColor: '#eee',
        tickFormatter(v) {
          return `${v}%`;
        }
      },
      xaxis: {
        show: false,
      },
      colors: ['#6ef146'],
      grid: {
        tickColor: '#eee',
        borderWidth: 0,
      }
    };

    const updateInterval = 30;
    const plot = $.plot($('#m_flotcharts_4'), [getRandomData()], options);

    function update() {
      plot.setData([getRandomData()]);
      plot.draw();
      setTimeout(update, updateInterval);
    }

    update();
  };

  const demo5 = function () {
    const d1 = [];
    for (var i = 0; i <= 10; i += 1) { d1.push([i, parseInt(Math.random() * 30)]); }

    const d2 = [];
    for (var i = 0; i <= 10; i += 1) { d2.push([i, parseInt(Math.random() * 30)]); }

    const d3 = [];
    for (var i = 0; i <= 10; i += 1) { d3.push([i, parseInt(Math.random() * 30)]); }

    let stack = 0;


    let bars = true;


    let lines = false;


    let steps = false;

    function plotWithOptions() {
      $.plot($('#m_flotcharts_5'),

        [{
          label: 'sales',
          data: d1,
          lines: {
            lineWidth: 1,
          },
          shadowSize: 0
        }, {
          label: 'tax',
          data: d2,
          lines: {
            lineWidth: 1,
          },
          shadowSize: 0
        }, {
          label: 'profit',
          data: d3,
          lines: {
            lineWidth: 1,
          },
          shadowSize: 0
        }],

        {
          series: {
            stack,
            lines: {
              show: lines,
              fill: true,
              steps,
              lineWidth: 0, // in pixels
            },
            bars: {
              show: bars,
              barWidth: 0.5,
              lineWidth: 0, // in pixels
              shadowSize: 0,
              align: 'center'
            }
          },
          grid: {
            tickColor: '#eee',
            borderColor: '#eee',
            borderWidth: 1
          }
        });
    }

    $('.stackControls input').click(function (e) {
      e.preventDefault();
      stack = $(this).val() == 'With stacking' ? true : null;
      plotWithOptions();
    });

    $('.graphControls input').click(function (e) {
      e.preventDefault();
      bars = $(this).val().indexOf('Bars') != -1;
      lines = $(this).val().indexOf('Lines') != -1;
      steps = $(this).val().indexOf('steps') != -1;
      plotWithOptions();
    });

    plotWithOptions();
  };

  const demo6 = function () {
    // bar chart:
    const data = GenerateSeries(0);

    function GenerateSeries(added) {
      const data = [];
      let start = 100 + added;
      let end = 200 + added;

      for (i = 1; i <= 20; i++) {
        const d = Math.floor(Math.random() * (end - start + 1) + start);
        data.push([i, d]);
        start++;
        end++;
      }

      return data;
    }

    const options = {
      series: {
        bars: {
          show: true
        }
      },
      bars: {
        barWidth: 0.8,
        lineWidth: 0, // in pixels
        shadowSize: 0,
        align: 'left'
      },

      grid: {
        tickColor: '#eee',
        borderColor: '#eee',
        borderWidth: 1
      }
    };

    $.plot($('#m_flotcharts_6'), [{
      data,
      lines: {
        lineWidth: 1,
      },
      shadowSize: 0
    }], options);
  };

  const demo7 = function () {
    // horizontal bar chart:

    const data1 = [
      [10, 10],
      [20, 20],
      [30, 30],
      [40, 40],
      [50, 50]
    ];

    const options = {
      series: {
        bars: {
          show: true
        }
      },
      bars: {
        horizontal: true,
        barWidth: 6,
        lineWidth: 0, // in pixels
        shadowSize: 0,
        align: 'left'
      },
      grid: {
        tickColor: '#eee',
        borderColor: '#eee',
        borderWidth: 1
      }
    };

    $.plot($('#m_flotcharts_7'), [data1], options);
  };


  const demo8 = function () {
    const data = [];
    let series = Math.floor(Math.random() * 10) + 1;
    series = series < 5 ? 5 : series;

    for (let i = 0; i < series; i++) {
      data[i] = {
        label: `Series${i + 1}`,
        data: Math.floor(Math.random() * 100) + 1
      };
    }

    $.plot($('#m_flotcharts_8'), data, {
      series: {
        pie: {
          show: true
        }
      }
    });
  };

  const demo9 = function () {
    const data = [];
    let series = Math.floor(Math.random() * 10) + 1;
    series = series < 5 ? 5 : series;

    for (let i = 0; i < series; i++) {
      data[i] = {
        label: `Series${i + 1}`,
        data: Math.floor(Math.random() * 100) + 1
      };
    }

    $.plot($('#m_flotcharts_9'), data, {
      series: {
        pie: {
          show: true
        }
      },
      legend: {
        show: false
      }
    });
  };

  const demo10 = function () {
    const data = [];
    let series = Math.floor(Math.random() * 10) + 1;
    series = series < 5 ? 5 : series;

    for (let i = 0; i < series; i++) {
      data[i] = {
        label: `Series${i + 1}`,
        data: Math.floor(Math.random() * 100) + 1
      };
    }

    $.plot($('#m_flotcharts_10'), data, {
      series: {
        pie: {
          show: true,
          radius: 1,
          label: {
            show: true,
            radius: 1,
            formatter(label, series) {
              return `<div style="font-size:8pt;text-align:center;padding:2px;color:white;">${label}<br/>${Math.round(series.percent)}%</div>`;
            },
            background: {
              opacity: 0.8
            }
          }
        }
      },
      legend: {
        show: false
      }
    });
  };

  const demo11 = function () {
    const data = [];
    let series = Math.floor(Math.random() * 10) + 1;
    series = series < 5 ? 5 : series;

    for (let i = 0; i < series; i++) {
      data[i] = {
        label: `Series${i + 1}`,
        data: Math.floor(Math.random() * 100) + 1
      };
    }

    $.plot($('#m_flotcharts_11'), data, {
      series: {
        pie: {
          show: true,
          radius: 1,
          label: {
            show: true,
            radius: 1,
            formatter(label, series) {
              return `<div style="font-size:8pt;text-align:center;padding:2px;color:white;">${label}<br/>${Math.round(series.percent)}%</div>`;
            },
            background: {
              opacity: 0.8
            }
          }
        }
      },
      legend: {
        show: false
      }
    });
  };


  return {
    // public functions
    init() {
      // default charts
      demo1();
      demo2();
      demo3();
      demo4();
      demo5();
      demo6();
      demo7();

      // pie charts
      demo8();
      demo9();
      demo10();
      demo11();
    }
  };
}());

jQuery(document).ready(() => {
  FlotchartsDemo.init();
});
