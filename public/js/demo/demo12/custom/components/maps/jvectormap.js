//= = Class definition
const jVectorMap = (function () {
  //= = Private functions

  const demo1 = function () {
  };

  return {
    // public functions
    init() {
      // default charts
      demo1();
    }
  };
}());

jQuery(document).ready(() => {
  jVectorMap.init();
});
