const DatatablesSearchOptionsAdvancedSearch = (function () {
  $.fn.dataTable.Api.register('column().title()', function () {
    return $(this.header()).text().trim();
  });

  const initTable1 = function () {
    // begin first table
    const table = $('#m_table_1').DataTable({
      responsive: true,
      //= = Pagination settings
      dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
      // read more: https://datatables.net/examples/basic_init/dom.html

      lengthMenu: [5, 10, 25, 50],

      pageLength: 10,

      language: {
        lengthMenu: 'Display _MENU_',
      },

      searchDelay: 500,
      processing: true,
      serverSide: true,
      ajax: {
        url: 'inc/api/datatables/demos/server.php',
        type: 'POST',
        data: {
          // parameters for custom backend script demo
          columnsDef: [
            'RecordID', 'OrderID', 'Country', 'ShipCity', 'CompanyAgent',
            'ShipDate', 'Status', 'Type', 'Actions', ],
        },
      },
      columns: [
        { data: 'RecordID' },
        { data: 'OrderID' },
        { data: 'Country' },
        { data: 'ShipCity' },
        { data: 'CompanyAgent' },
        { data: 'ShipDate' },
        { data: 'Status' },
        { data: 'Type' },
        { data: 'Actions' },
      ],

      initComplete() {
        this.api().columns().every(function () {
          const column = this;

          switch (column.title()) {
            case 'Country':
              column.data().unique().sort().each((d, j) => {
                $('.m-input[data-col-index="2"]').append(`<option value="${d}">${d}</option>`);
              });
              break;

            case 'Status':
              var status = {
                1: { title: 'Pending', class: 'm-badge--brand' },
                2: { title: 'Delivered', class: ' m-badge--metal' },
                3: { title: 'Canceled', class: ' m-badge--primary' },
                4: { title: 'Success', class: ' m-badge--success' },
                5: { title: 'Info', class: ' m-badge--info' },
                6: { title: 'Danger', class: ' m-badge--danger' },
                7: { title: 'Warning', class: ' m-badge--warning' },
              };
              column.data().unique().sort().each((d, j) => {
                $('.m-input[data-col-index="6"]').append(`<option value="${d}">${status[d].title}</option>`);
              });
              break;

            case 'Type':
              var status = {
                1: { title: 'Online', state: 'danger' },
                2: { title: 'Retail', state: 'primary' },
                3: { title: 'Direct', state: 'accent' },
              };
              column.data().unique().sort().each((d, j) => {
                $('.m-input[data-col-index="7"]').append(`<option value="${d}">${status[d].title}</option>`);
              });
              break;
          }
        });
      },

      columnDefs: [
        {
          targets: -1,
          title: 'Actions',
          orderable: false,
          render(data, type, full, meta) {
            return `
                        <span class="dropdown">
                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>
                                <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a>
                                <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>
                            </div>
                        </span>
                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                          <i class="la la-edit"></i>
                        </a>`;
          },
        },
        {
          targets: 6,
          render(data, type, full, meta) {
            const status = {
              1: { title: 'Pending', class: 'm-badge--brand' },
              2: { title: 'Delivered', class: ' m-badge--metal' },
              3: { title: 'Canceled', class: ' m-badge--primary' },
              4: { title: 'Success', class: ' m-badge--success' },
              5: { title: 'Info', class: ' m-badge--info' },
              6: { title: 'Danger', class: ' m-badge--danger' },
              7: { title: 'Warning', class: ' m-badge--warning' },
            };
            if (typeof status[data] === 'undefined') {
              return data;
            }
            return `<span class="m-badge ${status[data].class} m-badge--wide">${status[data].title}</span>`;
          },
        },
        {
          targets: 7,
          render(data, type, full, meta) {
            const status = {
              1: { title: 'Online', state: 'danger' },
              2: { title: 'Retail', state: 'primary' },
              3: { title: 'Direct', state: 'accent' },
            };
            if (typeof status[data] === 'undefined') {
              return data;
            }
            return `<span class="m-badge m-badge--${status[data].state} m-badge--dot"></span>&nbsp;`
							+ `<span class="m--font-bold m--font-${status[data].state}">${status[data].title}</span>`;
          },
        },
      ],
    });

    const filter = function () {
      const val = $.fn.dataTable.util.escapeRegex($(this).val());
      table.column($(this).data('col-index')).search(val || '', false, false).draw();
    };

    const asdasd = function (value, index) {
      const val = $.fn.dataTable.util.escapeRegex(value);
      table.column(index).search(val || '', false, true);
    };

    $('#m_search').on('click', (e) => {
      e.preventDefault();
      const params = {};
      $('.m-input').each(function () {
        const i = $(this).data('col-index');
        if (params[i]) {
          params[i] += `|${$(this).val()}`;
        } else {
          params[i] = $(this).val();
        }
      });
      $.each(params, (i, val) => {
        // apply search params to datatable
        table.column(i).search(val || '', false, false);
      });
      table.table().draw();
    });

    $('#m_reset').on('click', (e) => {
      e.preventDefault();
      $('.m-input').each(function () {
        $(this).val('');
        table.column($(this).data('col-index')).search('', false, false);
      });
      table.table().draw();
    });

    $('#m_datepicker').datepicker({
      todayHighlight: true,
      templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>',
      },
    });
  };

  return {

    // main function to initiate the module
    init() {
      initTable1();
    },

  };
}());

jQuery(document).ready(() => {
  DatatablesSearchOptionsAdvancedSearch.init();
});
