//= = Class definition

const BootstrapSelect = (function () {
  //= = Private functions
  const demos = function () {
    // minimum setup
    $('.m_selectpicker').selectpicker();
  };

  return {
    // public functions
    init() {
      demos();
    }
  };
}());

jQuery(document).ready(() => {
  BootstrapSelect.init();
});
