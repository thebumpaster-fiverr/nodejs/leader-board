//= = Class definition

const BootstrapSwitch = (function () {
  //= = Private functions
  const demos = function () {
    // minimum setup
    $('[data-switch=true]').bootstrapSwitch();
  };

  return {
    // public functions
    init() {
      demos();
    }
  };
}());

jQuery(document).ready(() => {
  BootstrapSwitch.init();
});
