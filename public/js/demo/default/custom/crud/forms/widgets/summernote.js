//= = Class definition

const SummernoteDemo = (function () {
  //= = Private functions
  const demos = function () {
    $('.summernote').summernote({
      height: 150
    });
  };

  return {
    // public functions
    init() {
      demos();
    }
  };
}());

//= = Initialization
jQuery(document).ready(() => {
  SummernoteDemo.init();
});
