//= = Class definition

const BootstrapTouchspin = (function () {
  //= = Private functions
  const demos = function () {
    // minimum setup
    // $('#m_timepicker_1, #m_timepicker_1_modal').timepicker();
  };

  return {
    // public functions
    init() {
      demos();
    }
  };
}());

jQuery(document).ready(() => {
  BootstrapTouchspin.init();
});
