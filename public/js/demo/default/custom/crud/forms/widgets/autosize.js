//= = Class definition

const Autosize = (function () {
  //= = Private functions
  const demos = function () {
    // basic demo
    const demo1 = $('#m_autosize_1');
    const demo2 = $('#m_autosize_2');

    autosize(demo1);

    autosize(demo2);
    autosize.update(demo2);
  };

  return {
    // public functions
    init() {
      demos();
    }
  };
}());

jQuery(document).ready(() => {
  Autosize.init();
});
