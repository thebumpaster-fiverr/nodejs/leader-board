//= = Class definition

const DropzoneDemo = (function () {
  //= = Private functions
  const demos = function () {
    // single file upload
    Dropzone.options.mDropzoneOne = {
      paramName: 'file', // The name that will be used to transfer the file
      maxFiles: 1,
      maxFilesize: 5, // MB
      addRemoveLinks: true,
      accept(file, done) {
        if (file.name == 'justinbieber.jpg') {
          done("Naha, you don't.");
        } else {
          done();
        }
      }
    };

    // multiple file upload
    Dropzone.options.mDropzoneTwo = {
      paramName: 'file', // The name that will be used to transfer the file
      maxFiles: 10,
      maxFilesize: 10, // MB
      addRemoveLinks: true,
      accept(file, done) {
        if (file.name == 'justinbieber.jpg') {
          done("Naha, you don't.");
        } else {
          done();
        }
      }
    };

    // file type validation
    Dropzone.options.mDropzoneThree = {
      paramName: 'file', // The name that will be used to transfer the file
      maxFiles: 10,
      maxFilesize: 10, // MB
      addRemoveLinks: true,
      acceptedFiles: 'image/*,application/pdf,.psd',
      accept(file, done) {
        if (file.name == 'justinbieber.jpg') {
          done("Naha, you don't.");
        } else {
          done();
        }
      }
    };
  };

  return {
    // public functions
    init() {
      demos();
    }
  };
}());

DropzoneDemo.init();
