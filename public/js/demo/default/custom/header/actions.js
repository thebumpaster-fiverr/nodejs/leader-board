//= = Class definition

const ActionsDemo = (function () {
  //= = Private functions

  return {
    // public functions
    init() {
      $('.summernote').summernote({
        height: 250,
      });
    }
  };
}());

//= = Initialization
jQuery(document).ready(() => {
  ActionsDemo.init();
});
