//= = Class definition

const DropdownDemo = (function () {
  //= = Private functions

  // basic demo
  const demo1 = function () {
    const output = $('#m_dropdown_api_output');
    const dropdown1 = new mDropdown('m_dropdown_api_1');
    const dropdown2 = new mDropdown('m_dropdown_api_2');

    dropdown1.on('afterShow', (dropdown) => {
      output.append('<p>Dropdown 1: afterShow event fired</p>');
    });
    dropdown1.on('beforeShow', (dropdown) => {
      output.append('<p>Dropdown 1: beforeShow event fired</p>');
    });
    dropdown1.on('afterHide', (dropdown) => {
      output.append('<p>Dropdown 1: afterHide event fired</p>');
    });
    dropdown1.on('beforeHide', (dropdown) => {
      output.append('<p>Dropdown 1: beforeHide event fired</p>');
    });

    dropdown2.on('afterShow', (dropdown) => {
      output.append('<p>Dropdown 2: afterShow event fired</p>');
    });
    dropdown2.on('beforeShow', (dropdown) => {
      output.append('<p>Dropdown 2: beforeShow event fired</p>');
    });
    dropdown2.on('afterHide', (dropdown) => {
      output.append('<p>Dropdown 2: afterHide event fired</p>');
    });
    dropdown2.on('beforeHide', (dropdown) => {
      output.append('<p>Dropdown 2: beforeHide event fired</p>');
    });
  };

  return {
    // public functions
    init() {
      demo1();
    }
  };
}());

jQuery(document).ready(() => {
  DropdownDemo.init();
});
