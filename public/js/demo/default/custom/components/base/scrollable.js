//= = Class definition

const Scrollable = (function () {
  //= = Private functions

  // basic demo
  const demo1 = function () {
  };

  return {
    // public functions
    init() {
      demo1();
    }
  };
}());

jQuery(document).ready(() => {
  Scrollable.init();
});
