//= = Class definition
const GoogleMapsDemo = (function () {
  //= = Private functions

  const demo1 = function () {
    const map = new GMaps({
      div: '#m_gmap_1',
      lat: -12.043333,
      lng: -77.028333
    });
  };

  const demo2 = function () {
    const map = new GMaps({
      div: '#m_gmap_2',
      zoom: 16,
      lat: -12.043333,
      lng: -77.028333,
      click(e) {
        alert('click');
      },
      dragend(e) {
        alert('dragend');
      }
    });
  };

  const demo3 = function () {
    const map = new GMaps({
      div: '#m_gmap_3',
      lat: -51.38739,
      lng: -6.187181,
    });
    map.addMarker({
      lat: -51.38739,
      lng: -6.187181,
      title: 'Lima',
      details: {
        database_id: 42,
        author: 'HPNeo'
      },
      click(e) {
        if (console.log) console.log(e);
        alert('You clicked in this marker');
      }
    });
    map.addMarker({
      lat: -12.042,
      lng: -77.028333,
      title: 'Marker with InfoWindow',
      infoWindow: {
        content: '<span style="color:#000">HTML Content!</span>'
      }
    });
    map.setZoom(5);
  };

  const demo4 = function () {
    const map = new GMaps({
      div: '#m_gmap_4',
      lat: -12.043333,
      lng: -77.028333
    });

    GMaps.geolocate({
      success(position) {
        map.setCenter(position.coords.latitude, position.coords.longitude);
      },
      error(error) {
        alert(`Geolocation failed: ${error.message}`);
      },
      not_supported() {
        alert('Your browser does not support geolocation');
      },
      always() {
        // alert("Geolocation Done!");
      }
    });
  };

  const demo5 = function () {
    const map = new GMaps({
      div: '#m_gmap_5',
      lat: -12.043333,
      lng: -77.028333,
      click(e) {
        console.log(e);
      }
    });

    path = [
      [-12.044012922866312, -77.02470665341184],
      [-12.05449279282314, -77.03024273281858],
      [-12.055122327623378, -77.03039293652341],
      [-12.075917129727586, -77.02764635449216],
      [-12.07635776902266, -77.02792530422971],
      [-12.076819390363665, -77.02893381481931],
      [-12.088527520066453, -77.0241058385925],
      [-12.090814532191756, -77.02271108990476]
    ];

    map.drawPolyline({
      path,
      strokeColor: '#131540',
      strokeOpacity: 0.6,
      strokeWeight: 6
    });
  };

  const demo6 = function () {
    const map = new GMaps({
      div: '#m_gmap_6',
      lat: -12.043333,
      lng: -77.028333
    });

    const path = [
      [-12.040397656836609, -77.03373871559225],
      [-12.040248585302038, -77.03993927003302],
      [-12.050047116528843, -77.02448169303511],
      [-12.044804866577001, -77.02154422636042]
    ];

    const polygon = map.drawPolygon({
      paths: path,
      strokeColor: '#BBD8E9',
      strokeOpacity: 1,
      strokeWeight: 3,
      fillColor: '#BBD8E9',
      fillOpacity: 0.6
    });
  };

  const demo7 = function () {
    const map = new GMaps({
      div: '#m_gmap_7',
      lat: -12.043333,
      lng: -77.028333
    });
    $('#m_gmap_7_btn').click((e) => {
      e.preventDefault();
      mUtil.scrollTo('m_gmap_7_btn', 400);
      map.travelRoute({
        origin: [-12.044012922866312, -77.02470665341184],
        destination: [-12.090814532191756, -77.02271108990476],
        travelMode: 'driving',
        step(e) {
          $('#m_gmap_7_routes').append(`<li>${e.instructions}</li>`);
          $(`#m_gmap_7_routes li:eq(${e.step_number})`).delay(800 * e.step_number).fadeIn(500, () => {
            map.setCenter(e.end_location.lat(), e.end_location.lng());
            map.drawPolyline({
              path: e.path,
              strokeColor: '#131540',
              strokeOpacity: 0.6,
              strokeWeight: 6
            });
          });
        }
      });
    });
  };

  const demo8 = function () {
    const map = new GMaps({
      div: '#m_gmap_8',
      lat: -12.043333,
      lng: -77.028333
    });

    const handleAction = function () {
      const text = $.trim($('#m_gmap_8_address').val());
      GMaps.geocode({
        address: text,
        callback(results, status) {
          if (status == 'OK') {
            const latlng = results[0].geometry.location;
            map.setCenter(latlng.lat(), latlng.lng());
            map.addMarker({
              lat: latlng.lat(),
              lng: latlng.lng()
            });
            mUtil.scrollTo('m_gmap_8');
          }
        }
      });
    };

    $('#m_gmap_8_btn').click((e) => {
      e.preventDefault();
      handleAction();
    });

    $('#m_gmap_8_address').keypress((e) => {
      const keycode = (e.keyCode ? e.keyCode : e.which);
      if (keycode == '13') {
        e.preventDefault();
        handleAction();
      }
    });
  };

  return {
    // public functions
    init() {
      // default charts
      demo1();
      demo2();
      demo3();
      demo4();
      demo5();
      demo6();
      demo7();
      demo8();
    }
  };
}());

jQuery(document).ready(() => {
  GoogleMapsDemo.init();
});
