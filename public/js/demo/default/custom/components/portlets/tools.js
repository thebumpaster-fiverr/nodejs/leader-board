const PortletTools = (function () {
  //= = Toastr
  const initToastr = function () {
    toastr.options.showDuration = 1000;
  };

  //= = Demo 1
  const demo1 = function () {
    // This portlet is lazy initialized using data-portlet="true" attribute. You can access to the portlet object as shown below and override its behavior
    const portlet = new mPortlet('m_portlet_tools_1');

    //= = Toggle event handlers
    portlet.on('beforeCollapse', (portlet) => {
      setTimeout(() => {
        toastr.info('Before collapse event fired!');
      }, 100);
    });

    portlet.on('afterCollapse', (portlet) => {
      setTimeout(() => {
        toastr.warning('Before collapse event fired!');
      }, 2000);
    });

    portlet.on('beforeExpand', (portlet) => {
      setTimeout(() => {
        toastr.info('Before expand event fired!');
      }, 100);
    });

    portlet.on('afterExpand', (portlet) => {
      setTimeout(() => {
        toastr.warning('After expand event fired!');
      }, 2000);
    });

    //= = Remove event handlers
    portlet.on('beforeRemove', (portlet) => {
      toastr.info('Before remove event fired!');

      return confirm('Are you sure to remove this portlet ?'); // remove portlet after user confirmation
    });

    portlet.on('afterRemove', (portlet) => {
      setTimeout(() => {
        toastr.warning('After remove event fired!');
      }, 2000);
    });

    //= = Reload event handlers
    portlet.on('reload', (portlet) => {
      toastr.info('Leload event fired!');

      mApp.block(portlet.getSelf(), {
        overlayColor: '#ffffff',
        type: 'loader',
        state: 'accent',
        opacity: 0.3,
        size: 'lg'
      });

      // update the content here

      setTimeout(() => {
        mApp.unblock(portlet.getSelf());
      }, 2000);
    });

    //= = Reload event handlers
    portlet.on('afterFullscreenOn', (portlet) => {
      toastr.warning('After fullscreen on event fired!');
      const scrollable = $(portlet.getBody()).find('> .m-scrollable');

      if (scrollable) {
        scrollable.data('original-height', scrollable.css('height'));
        scrollable.css('height', '100%');

        mUtil.scrollerUpdate(scrollable[0]);
      }
    });

    portlet.on('afterFullscreenOff', (portlet) => {
      toastr.warning('After fullscreen off event fired!');
      var scrollable = $(portlet.getBody()).find('> .m-scrollable');

      if (scrollable) {
        var scrollable = $(portlet.getBody()).find('> .m-scrollable');
        scrollable.css('height', scrollable.data('original-height'));

        mUtil.scrollerUpdate(scrollable[0]);
      }
    });
  };

  //= = Demo 2
  const demo2 = function () {
    // This portlet is lazy initialized using data-portlet="true" attribute. You can access to the portlet object as shown below and override its behavior
    const portlet = new mPortlet('m_portlet_tools_2');

    //= = Toggle event handlers
    portlet.on('beforeCollapse', (portlet) => {
      setTimeout(() => {
        toastr.info('Before collapse event fired!');
      }, 100);
    });

    portlet.on('afterCollapse', (portlet) => {
      setTimeout(() => {
        toastr.warning('Before collapse event fired!');
      }, 2000);
    });

    portlet.on('beforeExpand', (portlet) => {
      setTimeout(() => {
        toastr.info('Before expand event fired!');
      }, 100);
    });

    portlet.on('afterExpand', (portlet) => {
      setTimeout(() => {
        toastr.warning('After expand event fired!');
      }, 2000);
    });

    //= = Remove event handlers
    portlet.on('beforeRemove', (portlet) => {
      toastr.info('Before remove event fired!');

      return confirm('Are you sure to remove this portlet ?'); // remove portlet after user confirmation
    });

    portlet.on('afterRemove', (portlet) => {
      setTimeout(() => {
        toastr.warning('After remove event fired!');
      }, 2000);
    });

    //= = Reload event handlers
    portlet.on('reload', (portlet) => {
      toastr.info('Leload event fired!');

      mApp.block(portlet.getSelf(), {
        overlayColor: '#000000',
        type: 'spinner',
        state: 'brand',
        opacity: 0.05,
        size: 'lg'
      });

      // update the content here

      setTimeout(() => {
        mApp.unblock(portlet.getSelf());
      }, 2000);
    });
  };

  //= = Demo 3
  const demo3 = function () {
    // This portlet is lazy initialized using data-portlet="true" attribute. You can access to the portlet object as shown below and override its behavior
    const portlet = new mPortlet('m_portlet_tools_3');

    //= = Toggle event handlers
    portlet.on('beforeCollapse', (portlet) => {
      setTimeout(() => {
        toastr.info('Before collapse event fired!');
      }, 100);
    });

    portlet.on('afterCollapse', (portlet) => {
      setTimeout(() => {
        toastr.warning('Before collapse event fired!');
      }, 2000);
    });

    portlet.on('beforeExpand', (portlet) => {
      setTimeout(() => {
        toastr.info('Before expand event fired!');
      }, 100);
    });

    portlet.on('afterExpand', (portlet) => {
      setTimeout(() => {
        toastr.warning('After expand event fired!');
      }, 2000);
    });

    //= = Remove event handlers
    portlet.on('beforeRemove', (portlet) => {
      toastr.info('Before remove event fired!');

      return confirm('Are you sure to remove this portlet ?'); // remove portlet after user confirmation
    });

    portlet.on('afterRemove', (portlet) => {
      setTimeout(() => {
        toastr.warning('After remove event fired!');
      }, 2000);
    });

    //= = Reload event handlers
    portlet.on('reload', (portlet) => {
      toastr.info('Leload event fired!');

      mApp.block(portlet.getSelf(), {
        type: 'loader',
        state: 'success',
        message: 'Please wait...'
      });

      // update the content here

      setTimeout(() => {
        mApp.unblock(portlet.getSelf());
      }, 2000);
    });

    //= = Reload event handlers
    portlet.on('afterFullscreenOn', (portlet) => {
      toastr.warning('After fullscreen on event fired!');
      const scrollable = $(portlet.getBody()).find('> .m-scrollable');

      if (scrollable) {
        scrollable.data('original-height', scrollable.css('height'));
        scrollable.css('height', '100%');

        mUtil.scrollerUpdate(scrollable[0]);
      }
    });

    portlet.on('afterFullscreenOff', (portlet) => {
      toastr.warning('After fullscreen off event fired!');
      var scrollable = $(portlet.getBody()).find('> .m-scrollable');

      if (scrollable) {
        var scrollable = $(portlet.getBody()).find('> .m-scrollable');
        scrollable.css('height', scrollable.data('original-height'));

        mUtil.scrollerUpdate(scrollable[0]);
      }
    });
  };

  //= = Demo 4
  const demo4 = function () {
    // This portlet is lazy initialized using data-portlet="true" attribute. You can access to the portlet object as shown below and override its behavior
    const portlet = new mPortlet('m_portlet_tools_4');

    //= = Toggle event handlers
    portlet.on('beforeCollapse', (portlet) => {
      setTimeout(() => {
        toastr.info('Before collapse event fired!');
      }, 100);
    });

    portlet.on('afterCollapse', (portlet) => {
      setTimeout(() => {
        toastr.warning('Before collapse event fired!');
      }, 2000);
    });

    portlet.on('beforeExpand', (portlet) => {
      setTimeout(() => {
        toastr.info('Before expand event fired!');
      }, 100);
    });

    portlet.on('afterExpand', (portlet) => {
      setTimeout(() => {
        toastr.warning('After expand event fired!');
      }, 2000);
    });

    //= = Remove event handlers
    portlet.on('beforeRemove', (portlet) => {
      toastr.info('Before remove event fired!');

      return confirm('Are you sure to remove this portlet ?'); // remove portlet after user confirmation
    });

    portlet.on('afterRemove', (portlet) => {
      setTimeout(() => {
        toastr.warning('After remove event fired!');
      }, 2000);
    });

    //= = Reload event handlers
    portlet.on('reload', (portlet) => {
      toastr.info('Leload event fired!');

      mApp.block(portlet.getSelf(), {
        type: 'loader',
        state: 'brand',
        message: 'Please wait...'
      });

      // update the content here

      setTimeout(() => {
        mApp.unblock(portlet.getSelf());
      }, 2000);
    });

    //= = Reload event handlers
    portlet.on('afterFullscreenOn', (portlet) => {
      toastr.warning('After fullscreen on event fired!');
      const scrollable = $(portlet.getBody()).find('> .m-scrollable');

      if (scrollable) {
        scrollable.data('original-height', scrollable.css('height'));
        scrollable.css('height', '100%');

        mUtil.scrollerUpdate(scrollable[0]);
      }
    });

    portlet.on('afterFullscreenOff', (portlet) => {
      toastr.warning('After fullscreen off event fired!');
      var scrollable = $(portlet.getBody()).find('> .m-scrollable');

      if (scrollable) {
        var scrollable = $(portlet.getBody()).find('> .m-scrollable');
        scrollable.css('height', scrollable.data('original-height'));

        mUtil.scrollerUpdate(scrollable[0]);
      }
    });
  };

  //= = Demo 5
  const demo5 = function () {
    // This portlet is lazy initialized using data-portlet="true" attribute. You can access to the portlet object as shown below and override its behavior
    const portlet = new mPortlet('m_portlet_tools_5');

    //= = Toggle event handlers
    portlet.on('beforeCollapse', (portlet) => {
      setTimeout(() => {
        toastr.info('Before collapse event fired!');
      }, 100);
    });

    portlet.on('afterCollapse', (portlet) => {
      setTimeout(() => {
        toastr.warning('Before collapse event fired!');
      }, 2000);
    });

    portlet.on('beforeExpand', (portlet) => {
      setTimeout(() => {
        toastr.info('Before expand event fired!');
      }, 100);
    });

    portlet.on('afterExpand', (portlet) => {
      setTimeout(() => {
        toastr.warning('After expand event fired!');
      }, 2000);
    });

    //= = Remove event handlers
    portlet.on('beforeRemove', (portlet) => {
      toastr.info('Before remove event fired!');

      return confirm('Are you sure to remove this portlet ?'); // remove portlet after user confirmation
    });

    portlet.on('afterRemove', (portlet) => {
      setTimeout(() => {
        toastr.warning('After remove event fired!');
      }, 2000);
    });

    //= = Reload event handlers
    portlet.on('reload', (portlet) => {
      toastr.info('Leload event fired!');

      mApp.block(portlet.getSelf(), {
        type: 'loader',
        state: 'brand',
        message: 'Please wait...'
      });

      // update the content here

      setTimeout(() => {
        mApp.unblock(portlet.getSelf());
      }, 2000);
    });

    //= = Reload event handlers
    portlet.on('afterFullscreenOn', (portlet) => {
      toastr.info('After fullscreen on event fired!');
    });

    portlet.on('afterFullscreenOff', (portlet) => {
      toastr.warning('After fullscreen off event fired!');
    });
  };

  //= = Demo 5
  const demo6 = function () {
    // This portlet is lazy initialized using data-portlet="true" attribute. You can access to the portlet object as shown below and override its behavior
    const portlet = new mPortlet('m_portlet_tools_6');

    //= = Toggle event handlers
    portlet.on('beforeCollapse', (portlet) => {
      setTimeout(() => {
        toastr.info('Before collapse event fired!');
      }, 100);
    });

    portlet.on('afterCollapse', (portlet) => {
      setTimeout(() => {
        toastr.warning('Before collapse event fired!');
      }, 2000);
    });

    portlet.on('beforeExpand', (portlet) => {
      setTimeout(() => {
        toastr.info('Before expand event fired!');
      }, 100);
    });

    portlet.on('afterExpand', (portlet) => {
      setTimeout(() => {
        toastr.warning('After expand event fired!');
      }, 2000);
    });

    //= = Remove event handlers
    portlet.on('beforeRemove', (portlet) => {
      toastr.info('Before remove event fired!');

      return confirm('Are you sure to remove this portlet ?'); // remove portlet after user confirmation
    });

    portlet.on('afterRemove', (portlet) => {
      setTimeout(() => {
        toastr.warning('After remove event fired!');
      }, 2000);
    });

    //= = Reload event handlers
    portlet.on('reload', (portlet) => {
      toastr.info('Leload event fired!');

      mApp.block(portlet.getSelf(), {
        type: 'loader',
        state: 'brand',
        message: 'Please wait...'
      });

      // update the content here

      setTimeout(() => {
        mApp.unblock(portlet.getSelf());
      }, 2000);
    });

    //= = Reload event handlers
    portlet.on('afterFullscreenOn', (portlet) => {
      toastr.info('After fullscreen on event fired!');
    });

    portlet.on('afterFullscreenOff', (portlet) => {
      toastr.warning('After fullscreen off event fired!');
    });
  };

  return {
    // main function to initiate the module
    init() {
      initToastr();

      // init demos
      demo1();
      demo2();
      demo3();
      demo4();
      demo5();
      demo6();
    }
  };
}());

jQuery(document).ready(() => {
  PortletTools.init();
});
