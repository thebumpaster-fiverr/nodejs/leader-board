const mLayout = (function () {
  let header;
  let horMenu;
  let asideMenu;
  let asideMenuOffcanvas;
  let horMenuOffcanvas;
  let asideLeftToggle;
  let asideLeftHide;
  let scrollTop;
  let quicksearch;
  let mainPortlet;

  //= = Header
  const initStickyHeader = function () {
    let tmp;
    const headerEl = mUtil.get('m_header');
    const options = {
      offset: {},
      minimize: {}
    };

    if (mUtil.attr(headerEl, 'm-minimize-mobile') == 'hide') {
      options.minimize.mobile = {};
      options.minimize.mobile.on = 'm-header--hide';
      options.minimize.mobile.off = 'm-header--show';
    } else {
      options.minimize.mobile = false;
    }

    if (mUtil.attr(headerEl, 'm-minimize') == 'hide') {
      options.minimize.desktop = {};
      options.minimize.desktop.on = 'm-header--hide';
      options.minimize.desktop.off = 'm-header--show';
    } else {
      options.minimize.desktop = false;
    }

    if (tmp = mUtil.attr(headerEl, 'm-minimize-offset')) {
      options.offset.desktop = tmp;
    }

    if (tmp = mUtil.attr(headerEl, 'm-minimize-mobile-offset')) {
      options.offset.mobile = tmp;
    }

    header = new mHeader('m_header', options);
  };

  //= = Hor menu
  const initHorMenu = function () {
    // init aside left offcanvas
    horMenuOffcanvas = new mOffcanvas('m_header_menu', {
      overlay: true,
      baseClass: 'm-aside-header-menu-mobile',
      closeBy: 'm_aside_header_menu_mobile_close_btn',
      toggleBy: {
        target: 'm_aside_header_menu_mobile_toggle',
        state: 'm-brand__toggler--active'
      }
    });

    horMenu = new mMenu('m_header_menu', {
      submenu: {
        desktop: 'dropdown',
        tablet: 'accordion',
        mobile: 'accordion'
      },
      accordion: {
        slideSpeed: 200, // accordion toggle slide speed in milliseconds
        expandAll: false // allow having multiple expanded accordions in the menu
      }
    });
  };

  //= = Aside menu
  const initLeftAsideMenu = function () {
    //= = Init aside menu
    const menu = mUtil.get('m_ver_menu');
    const menuDesktopMode = (mUtil.attr(menu, 'm-menu-dropdown') === '1' ? 'dropdown' : 'accordion');

    let scroll;
    if (mUtil.attr(menu, 'm-menu-scrollable') === '1') {
      scroll = {
        height() {
          if (mUtil.isInResponsiveRange('desktop')) {
            return mUtil.getViewPort().height - parseInt(mUtil.css('m_header', 'height'));
          }
        }
      };
    }

    asideMenu = new mMenu('m_ver_menu', {
      // vertical scroll
      scroll,

      // submenu setup
      submenu: {
        desktop: {
          // by default the menu mode set to accordion in desktop mode
          default: menuDesktopMode,
          // whenever body has this class switch the menu mode to dropdown
          state: {
            body: 'm-aside-left--minimize',
            mode: 'dropdown'
          }
        },
        tablet: 'accordion', // menu set to accordion in tablet mode
        mobile: 'accordion' // menu set to accordion in mobile mode
      },

      // accordion setup
      accordion: {
        autoScroll: false, // enable auto scrolling(focus) to the clicked menu item
        expandAll: false // allow having multiple expanded accordions in the menu
      }
    });
  };

  //= = Aside
  const initLeftAside = function () {
    // init aside left offcanvas
    const body = mUtil.get('body');
    const asideLeft = mUtil.get('m_aside_left');
    const asideOffcanvasClass = mUtil.hasClass(asideLeft, 'm-aside-left--offcanvas-default') ? 'm-aside-left--offcanvas-default' : 'm-aside-left';

    asideMenuOffcanvas = new mOffcanvas('m_aside_left', {
      baseClass: asideOffcanvasClass,
      overlay: true,
      closeBy: 'm_aside_left_close_btn',
      toggleBy: {
        target: 'm_aside_left_offcanvas_toggle',
        state: 'm-brand__toggler--active'
      }
    });

    //= = Handle minimzied aside hover
    if (mUtil.hasClass(body, 'm-aside-left--fixed')) {
      let insideTm;
      let outsideTm;

      mUtil.addEvent(asideLeft, 'mouseenter', () => {
        if (outsideTm) {
          clearTimeout(outsideTm);
          outsideTm = null;
        }

        insideTm = setTimeout(() => {
          if (mUtil.hasClass(body, 'm-aside-left--minimize') && mUtil.isInResponsiveRange('desktop')) {
            mUtil.removeClass(body, 'm-aside-left--minimize');
            mUtil.addClass(body, 'm-aside-left--minimize-hover');
            asideMenu.scrollerUpdate();
            asideMenu.scrollerTop();
          }
        }, 300);
      });

      mUtil.addEvent(asideLeft, 'mouseleave', () => {
        if (insideTm) {
          clearTimeout(insideTm);
          insideTm = null;
        }

        outsideTm = setTimeout(() => {
          if (mUtil.hasClass(body, 'm-aside-left--minimize-hover') && mUtil.isInResponsiveRange('desktop')) {
            mUtil.removeClass(body, 'm-aside-left--minimize-hover');
            mUtil.addClass(body, 'm-aside-left--minimize');
            asideMenu.scrollerUpdate();
            asideMenu.scrollerTop();
          }
        }, 500);
      });
    }
  };

  //= = Sidebar toggle
  const initLeftAsideToggle = function () {
    if ($('#m_aside_left_minimize_toggle').length === 0) {
      return;
    }

    asideLeftToggle = new mToggle('m_aside_left_minimize_toggle', {
      target: 'body',
      targetState: 'm-brand--minimize m-aside-left--minimize',
      togglerState: 'm-brand__toggler--active'
    });

    asideLeftToggle.on('toggle', (toggle) => {
      if (mUtil.get('main_portlet')) {
        mainPortlet.updateSticky();
      }

      horMenu.pauseDropdownHover(800);
      asideMenu.pauseDropdownHover(800);

      //= = Remember state in cookie
      Cookies.set('sidebar_toggle_state', toggle.getState());
      // to set default minimized left aside use this cookie value in your
      // server side code and add "m-brand--minimize m-aside-left--minimize" classes to
      // the body tag in order to initialize the minimized left aside mode during page loading.
    });

    asideLeftToggle.on('beforeToggle', (toggle) => {
      const body = mUtil.get('body');
      if (mUtil.hasClass(body, 'm-aside-left--minimize') === false && mUtil.hasClass(body, 'm-aside-left--minimize-hover')) {
        mUtil.removeClass(body, 'm-aside-left--minimize-hover');
      }
    });
  };

  //= = Sidebar hide
  var initLeftAsideHide = function () {
    if ($('#m_aside_left_hide_toggle').length === 0) {
      return;
    }

    initLeftAsideHide = new mToggle('m_aside_left_hide_toggle', {
      target: 'body',
      targetState: 'm-aside-left--hide',
      togglerState: 'm-brand__toggler--active'
    });

    initLeftAsideHide.on('toggle', (toggle) => {
      horMenu.pauseDropdownHover(800);
      asideMenu.pauseDropdownHover(800);

      //= = Remember state in cookie
      Cookies.set('sidebar_hide_state', toggle.getState());
      // to set default minimized left aside use this cookie value in your
      // server side code and add "m-brand--minimize m-aside-left--minimize" classes to
      // the body tag in order to initialize the minimized left aside mode during page loading.
    });
  };

  //= = Topbar
  const initTopbar = function () {
    $('#m_aside_header_topbar_mobile_toggle').click(() => {
      $('body').toggleClass('m-topbar--on');
    });

    // Animated Notification Icon
    /*
        setInterval(function() {
            $('#m_topbar_notification_icon .m-nav__link-icon').addClass('m-animate-shake');
            $('#m_topbar_notification_icon .m-nav__link-badge').addClass('m-animate-blink');
        }, 3000);

        setInterval(function() {
            $('#m_topbar_notification_icon .m-nav__link-icon').removeClass('m-animate-shake');
            $('#m_topbar_notification_icon .m-nav__link-badge').removeClass('m-animate-blink');
        }, 6000);
        */
  };

  //= = Quicksearch
  const initQuicksearch = function () {
    if ($('#m_quicksearch').length === 0) {
      return;
    }

    quicksearch = new mQuicksearch('m_quicksearch', {
      mode: mUtil.attr('m_quicksearch', 'm-quicksearch-mode'), // quick search type
      minLength: 1
    });

    // <div class="m-search-results m-search-results--skin-light"><span class="m-search-result__message">Something went wrong</div></div>

    quicksearch.on('search', (the) => {
      the.showProgress();

      $.ajax({
        url: 'inc/api/quick_search.php',
        data: {
          query: the.query
        },
        dataType: 'html',
        success(res) {
          the.hideProgress();
          the.showResult(res);
        },
        error(res) {
          the.hideProgress();
          the.showError('Connection error. Pleae try again later.');
        }
      });
    });
  };

  //= = Scrolltop
  const initScrollTop = function () {
    const scrollTop = new mScrollTop('m_scroll_top', {
      offset: 300,
      speed: 600
    });
  };

  //= = Main portlet(sticky portlet)
  const createMainPortlet = function () {
    return new mPortlet('main_portlet', {
      sticky: {
        offset: parseInt(mUtil.css(mUtil.get('m_header'), 'height')),
        zIndex: 90,
        position: {
          top() {
            return parseInt(mUtil.css(mUtil.get('m_header'), 'height'));
          },
          left() {
            let left = parseInt(mUtil.css(mUtil.getByClass('m-content'), 'paddingLeft'));

            if (mUtil.isInResponsiveRange('desktop')) {
              // left += parseInt(mUtil.css(mUtil.get('m_aside_left'), 'width') );
              if (mUtil.hasClass(mUtil.get('body'), 'm-aside-left--minimize')) {
                left += 78; // need to use hardcoded width of the minimize aside
              } else {
                left += 255; // need to use hardcoded width of the aside
              }
            }

            return left;
          },
          right() {
            return parseInt(mUtil.css(mUtil.getByClass('m-content'), 'paddingRight'));
          }
        }
      }
    });
  };

  return {
    init() {
      this.initHeader();
      this.initAside();
      this.initMainPortlet();
    },

    initMainPortlet() {
      if (!mUtil.get('main_portlet')) {
        return;
      }

      mainPortlet = createMainPortlet();
      mainPortlet.initSticky();

      mUtil.addResizeHandler(() => {
        mainPortlet.updateSticky();
      });
    },

    resetMainPortlet() {
      mainPortlet.destroySticky();
      mainPortlet = createMainPortlet();
      mainPortlet.initSticky();
    },

    initHeader() {
      initStickyHeader();
      initHorMenu();
      initTopbar();
      initQuicksearch();
      initScrollTop();
    },

    initAside() {
      initLeftAside();
      initLeftAsideMenu();
      initLeftAsideToggle();
      initLeftAsideHide();

      this.onLeftSidebarToggle((e) => {
        //= = Update sticky portlet
        if (mainPortlet) {
          mainPortlet.updateSticky();
        }

        //= = Reload datatable
        const datatables = $('.m-datatable');
        if (datatables) {
          datatables.each(function () {
            $(this).mDatatable('redraw');
          });
        }
      });
    },

    getAsideMenu() {
      return asideMenu;
    },

    onLeftSidebarToggle(handler) {
      if (asideLeftToggle) {
        asideLeftToggle.on('toggle', handler);
      }
    },

    closeMobileAsideMenuOffcanvas() {
      if (mUtil.isMobileDevice()) {
        asideMenuOffcanvas.hide();
      }
    },

    closeMobileHorMenuOffcanvas() {
      if (mUtil.isMobileDevice()) {
        horMenuOffcanvas.hide();
      }
    }
  };
}());

$(document).ready(() => {
  if (mUtil.isAngularVersion() === false) {
    mLayout.init();
  }
});
