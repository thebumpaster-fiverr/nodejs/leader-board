//= = Class definition

const DatatableHtmlTableDemo = (function () {
  //= = Private functions

  // demo initializer
  const demo = function () {
    const datatable = $('.m-datatable').mDatatable({
      search: {
        input: $('#generalSearch')
      },
      layout: {
        scroll: true,
        height: 400
      },
      columns: [
        {
          field: 'DepositPaid',
          type: 'number',
          locked: { left: 'xl' }
        },
        {
          field: 'OrderDate',
          type: 'date',
          format: 'YYYY-MM-DD',
          locked: { left: 'xl' }
        }
      ]
    });
  };

  return {
    //= = Public functions
    init() {
      // init dmeo
      demo();
    }
  };
}());

jQuery(document).ready(() => {
  DatatableHtmlTableDemo.init();
});
